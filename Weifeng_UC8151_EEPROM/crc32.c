//----------------------------------------------------------------------------
// crc32.c
//----------------------------------------------------------------------------
#include "all_includes.h"

#include "crc32.h"


//	32-bit CRC (cyclic redundancy check) Generator
//  Polynomial:						04C11DB7
//  Initial CRC register value:		FFFFFFFF
//  Reflected input and output:		No
//  Inverted final output:			No
//  CRC of CStdString "123456789":  0376E6E7

#define INITIAL_VALUE		0xFFFFFFFFU
#define CRC32_POLYNOMIAL	0x04c11db7U

//uint_32 m_crc;

void crc32_init(uint32_t *crc) {
	*crc = INITIAL_VALUE;
}



void crc32_compute(uint32_t *crc, uint8_t data) {
	int i;
	uint32_t value = ((uint32_t)data) & 0x0FFU;

	*crc ^= (value << 24);
	for (i = 0; i < 8; i++) {
		if ((*crc) & 0x80000000U) {
			(*crc) = ((*crc) << 1) ^ CRC32_POLYNOMIAL;
		} else {
			(*crc) <<= 1;
		}
	}
}


