//----------------------------------------------------------------------------
// communications.h
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// 24-bit address space is divided as follows:
// 0x000000 - 0x0007FF: Control registers
// 0x000800 - 0x000FFF: Provisioning storage in NVM
// 0x001000 - 0x00FFFF: Reserved
// 0x010000 - 0xFFFFFF: Image data
//----------------------------------------------------------------------------
#define REGISTER_DISPLAY_TYPE               0		// 0x00
#define REGISTER_FIRMWARE_VERSION           1		// 0x04
#define REGISTER_STATUS                     2		// 0x08
#define REGISTER_IMMEDIATE_RENDER           3		// 0x0C
#define REGISTER_CLEAR_SCREENS              4		// 0x10
#define REGISTER_NETWORK_TIME               5		// 0x14
#define REGISTER_SCHEDULED_DISPLAY_EVENT    6		// 0x18
#define REGISTER_IMMEDIATE_DISPLAY_EVENT    7		// 0x1C
#define REGISTER_SEQUENCE_EVENT_START_TIME  8		// 0x20
#define REGISTER_SEQUENCE_SCREEN_ORDER      9		// 0x24
#define REGISTER_SEQUENCE_CONFIG           10		// 0x28
#define REGISTER_SEQUENCE_SCREEN_01_TIME   11		// 0x2C
#define REGISTER_SEQUENCE_SCREEN_23_TIME   12		// 0x30
#define REGISTER_SEQUENCE_SCREEN_45_TIME   13		// 0x34
#define REGISTER_SEQUENCE_SCREEN_67_TIME   14		// 0x38
#define REGISTER_SEQUENCE_SCREEN_89_TIME   15		// 0x3C
#define REGISTER_RENDER_COUNT              16		// 0x40
#define REGISTER_TEMPERATURE               17		// 0x44
#define REGISTER_RENDER_VOLTAGE            18		// 0x48
#define REGISTER_COPY_SCREEN	           19		// 0x4C
#define REGISTER_CRC_CMD	 	   20		// 0x50
#define REGISTER_CRC_RESULT	 	   21		// 0x54
#define REGISTER_DEV_PARTIAL 	           22		// 0x58
#define REGISTER_MEMORY_MAP 	           23		// 0x5C
#define REGISTER_PROVISIONING 	           24		// 0x60
#define	REGISTER_DEBUG				25	// 0x64

#define NUM_REGISTERS                      26


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
extern void communications_initialize(void);
extern void communications_process(void);
extern void communications_transfer_start(uint32_t address);
extern void communications_transfer_update(uint8_t value);
extern void communications_transfer_end(void);
//extern void communications_set_memory(uint32_t address,uint8_t value);
extern uint8_t communications_get_memory(uint32_t address);
extern void communications_set_register(uint8_t reg,uint32_t value);
extern uint32_t communications_get_register(uint8_t reg);
extern uint32_t communications_reverse_long(uint32_t value);
//extern uint16_t communications_reverse_short(uint16_t value);
