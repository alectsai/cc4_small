#ifndef _INCLUDE_ERROR_H
#define _INCLUDE_ERROR_H

//----------------------------------------------------------------------------
// error.h - this include defines for error reporting
//----------------------------------------------------------------------------
// These values are written to the CC4 status registers.

//---------------------------------------------------------------------
// status register bit location
// bit 8: flash write address too large
// bit 7: internal reset requested by execution of illegal instruction	RL78 RESF bit 7 - TRAP
// bit 4: internal reset requested by watchdog time			RL78 RESF bit 4 - WDTRF
// bit 2: internal reset requested by RAM parity			RL78 RESF bit 2 - RPERF
// bit 1: internal reset requested by illegal-memory access		RL78 RESF bit 1 - IAWRF
// bit 0: internal reset requested by voltage detector			RL78 RESF bit 0 - LVIRF

#define ERROR_FLASH_WR_ADR_TOO_BIG	0x00000100UL	// the flash address received to write is too large for the device
#define ERROR_FLASH_WR_PROTECT		0x00000200UL	// flash was write protect when trying to write to it
#define ERROR_FLASH_WRONG_TYPE		0x00000400UL	// flash is not Adesto or wrong family code or Density code
#define ERROR_COM_NO_CC4		0x00000800UL	// cannot communicate with CC4
#define ERROR_INVALID_TYPE		0x00001000UL	// Assgigned tag type is invalid

// error bits used in 5.83" tags
#define ERROR_RESET			0x80000000UL	// Flag for non-zero RESF at boot
#define ERROR_RENDER_TIMEOUT_POWERON	0x40000000UL	// Busy T/O after power on command
#define ERROR_RENDER_TIMEOUT_POWEROFF	0x20000000UL
#define ERROR_RENDER_TIMEOUT_RENDER	0x10000000UL
#define ERROR_RENDER_TIMEOUT_PSR	0x08000000UL
#define ERROR_RENDER_TIMEOUT_TRES	0x04000000UL
#define ERROR_RENDER_TIMEOUT_READ_TEMP	0x02000000UL
#define ERROR_RENDER_TIMEOUT_TSC	0x01000000UL

#define	STATUS_RENDER_STATE_MASK	0xFF1FFFFFUL	// Bits 23:21 are render state
#define STATUS_RENDER_STATE_INIT	0x00800000UL	// Initial value - no renders performed
#define STATUS_RENDER_STATE_RENDERING	0x00A00000UL	// Rendering state, while "display_render()" is running
#define STATUS_RENDER_STATE_RENDER_FAIL	0x00C00000UL	// Render failed
#define STATUS_RENDER_STATE_RENDER_PASS	0x00E00000UL	// Render completed

#define	STATUS_RENDER_FAIL_COUNT_SHIFT	13L		// Shift and mask for extracting render fail count
#define	STATUS_RENDER_FAIL_COUNT_MASK	0xFFE01FFFUL	// from status register for EEPROM storage

void error_report(uint32_t errNum);


#endif
