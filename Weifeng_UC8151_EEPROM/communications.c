//----------------------------------------------------------------------------
// communications.c
//----------------------------------------------------------------------------
#include "all_includes.h"
#include "config.h"
#include "memory.h"
#include "provisioning.h"



//----------------------------------------------------------------------------
// The I2C interface for this system allows transactions of up to 48 bytes.
// An entire transaction is buffered before transfering it to the registers.
//----------------------------------------------------------------------------
#define TRANSFER_BUFFER_SIZE 48

#define MEMORY_PERSISTENT_MEMORY_OFFSET EEPROM_PERSISTENT_MEMORY_OFFSET

uint32_t transfer_address;
uint8_t transfer_count;
uint8_t transfer_data[TRANSFER_BUFFER_SIZE];

uint32_t previous_transfer_address = 0xFFFFFFFF;
uint8_t previous_transfer_count;
uint8_t previous_transfer_data[TRANSFER_BUFFER_SIZE];



//----------------------------------------------------------------------------
// NOTE: long integers are stored in reverse byte order from what CC4
// uses. Thus, the set/get register functions, used primarily by the event
// manager, perform the appropriate byte swapping. This lessens the burdon
// on the I2C interrupts that must enter register data via a bytestream.
//
// However, it also means the lwords in this table must be entered in
// reverse byte order.
//----------------------------------------------------------------------------
uint32_t registers[NUM_REGISTERS];

const uint32_t register_read_only_masks[NUM_REGISTERS] = {
	0x0000FFFF, // 0xFFFF0000 - REGISTER_DISPLAY_TYPE
	0xFFFFFFFF, // 0xFFFFFFFF - REGISTER_FIRMWARE_VERSION
	0xFFFFFFFF, // 0xFFFFFFFF - REGISTER_STATUS
	0xFCF0FFFF, // 0xFFFFF0FC - REGISTER_IMMEDIATE_RENDER
	0xF80000FF, // 0xFF0000F8 - REGISTER_CLEAR_SCREENS
	0x00000000, // 0x00000000 - REGISTER_NETWORK_TIME
	0xE0FF0000, // 0x0000FFE0 - REGISTER_SCHEDULED_DISPLAY_EVENT
	0xA0FF0000, // 0x0000FFA0 - REGISTER_IMMEDIATE_DISPLAY_EVENT
	0x1C000000, // 0x0000001C - REGISTER_SEQUENCE_EVENT_START_TIME
	0x00000000, // 0x00000000 - REGISTER_SEQUENCE_SCREEN_ORDER
	0xF00FFF00, // 0x00FF0FF0 - REGISTER_SEQUENCE_CONFIG
	0x00000000, // 0x00000000 - REGISTER_SEQUENCE_SCREEN_01_TIME
	0x00000000, // 0x00000000 - REGISTER_SEQUENCE_SCREEN_23_TIME
	0x00000000, // 0x00000000 - REGISTER_SEQUENCE_SCREEN_45_TIME
	0x00000000, // 0x00000000 - REGISTER_SEQUENCE_SCREEN_67_TIME
	0x00000000, // 0x00000000 - REGISTER_SEQUENCE_SCREEN_89_TIME
	0xFFFFFFFF, // 0xFFFFFFFF - REGISTER_RENDER_COUNT
	0x00000000, // 0xFFFFFFFF - REGISTER_TEMPERATURE
	0x00000000, // 0x00000000 - REGISTER_RENDER_VOLTAGE
	0x00000000, // 0x00000000 - REGISTER_SCREEN_COPY
	0x00000000, // 0x00000000 - REGISTER_CRC_CMD
	0xFFFFFFFF, // 0xFFFFFFFF - REGISTER_CRC_RESULT
	0x00000000, // 0x00000000 - REGISTER_DEV_PARTIAL
	0x00000000, // 0x00000000 - REGISTER_MEMORY_MAP
	0x000000FF  // 0xFF000000 - REGISTER_PROVISIONING
};



//----------------------------------------------------------------------------
// 1 x 64Kbytes for register address space.
// 3 x 64KBytes for image address space.
// 3 x 64Kbytes for RLE image address space.
// 1 x 64Kbytes reserved.
//
// 5 MS bits of 24-bit address space are reserved.
//----------------------------------------------------------------------------



//----------------------------------------------------------------------------
// Because of latency when writing to the memory, an additional layer
// of buffering is needed.
//
// The circular data buffer is used to buffer image data (both uncompressed
// and RLE) from the I2C (interrupt level) to the memory via the event
// manager (user level).
//
// The first byte of an entry specifies the number of data bytes.
// Thus, up to 255 bytes of data per transaction.
//
// The eext three bytes specify the memory address. This address has already
// has the image data address offset subtracted from it. These three bytes
// are not included in the data byte count.
//
// Circular buffer wrap-arounds are not allowed so the circular buffer is
// extended by the transfer buffer size plus 4.
//----------------------------------------------------------------------------
#define IMAGE_DATA_BUFFER_SIZE 256

volatile uint16_t image_data_buffer_head;
volatile uint16_t image_data_buffer_tail;
volatile uint16_t image_data_buffer_count;
uint8_t image_data_buffer[IMAGE_DATA_BUFFER_SIZE + TRANSFER_BUFFER_SIZE + 4];



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void communications_initialize(void) {
	uint8_t reset_source;

	communications_set_register(REGISTER_FIRMWARE_VERSION, FIRMWARE_VERSION);	 

	// Use "reset_source" to fetch failed render count from EEPROM
	eeprom_read( PROVISIONING_RENDER_FAIL_COUNT_OFFSET, &reset_source, 1 );

	// Setup initial status register contents
	communications_set_register( REGISTER_STATUS, (STATUS_RENDER_STATE_INIT|(((uint32_t)reset_source)<<STATUS_RENDER_FAIL_COUNT_SHIFT)) );

	//
	// capture the reset source if set
	//
	// [7] TRAP	- Illegal instruction
	// [4] WDTRF	- WDT Timeout
	// [2] RPERF	- RAM parity error
	// [1] IAWRF	- Illegal memory access
	// [0] LVIRF	- Low Voltage
	reset_source = RESF;

	if (reset_source != 0x00) {
		// it would be nice if we can get the status register to survive non-POR events
		// be ready for it..
		uint32_t error =	communications_get_register(REGISTER_STATUS);
		error &= 0xFFFFFF00;
		error |= ( ERROR_RESET | reset_source );

		// If the MCU has been reset by anything other that POR, post it in the status
		communications_set_register(REGISTER_STATUS, error);
	}

	communications_set_register(REGISTER_SEQUENCE_SCREEN_01_TIME,0x00010001);
	communications_set_register(REGISTER_SEQUENCE_SCREEN_23_TIME,0x00010001);
	communications_set_register(REGISTER_SEQUENCE_SCREEN_45_TIME,0x00010001);
	communications_set_register(REGISTER_SEQUENCE_SCREEN_67_TIME,0x00010001);
	communications_set_register(REGISTER_SEQUENCE_SCREEN_89_TIME,0x00010001);
	communications_set_register(REGISTER_TEMPERATURE,21000);
	communications_set_register(REGISTER_RENDER_VOLTAGE,0x0000FF00);
	//communications_set_register(REGISTER_MEMORY_MAP,0x00000000);

	// Read REGISTER_RENDER_COUNT from EEPROM
	eeprom_read( PROVISIONING_RENDER_COUNT_OFFSET, (uint8_t *)&registers[REGISTER_RENDER_COUNT], 4 );

}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void communications_process(void) {
	uint32_t value;
	uint32_t address;
	uint8_t count;
	uint8_t i;
	uint8_t output_buffer[16];
	uint8_t index;
	uint8_t bit;
	uint8_t byte;
	uint8_t pixel_count;
	uint8_t rle_value;
	uint16_t data_count;

	uint8_t mmap = communications_get_register(REGISTER_MEMORY_MAP);

	// Adjust network time, if needed
	while (timer_increment) {
		// This must be completed as a single atomic action.
		__disable_interrupt();

		value	= communications_reverse_long(registers[REGISTER_NETWORK_TIME]);
		value += 250;
		registers[REGISTER_NETWORK_TIME] = communications_reverse_long(value);

		if (eeprom_power_timeout > 0) {
			--eeprom_power_timeout;
		}

		--timer_increment;

		__enable_interrupt();

		++display_time_since_last_render;
		++temperature_sampling_count;
		++cc4_abl_kick_count;
		++cc4_no_sync_check;
		++provisioning_check;
		++cc4_tag_id_check;
	}

	count = 0;
	while (1) {

		// adjustment of data buffer count must be atomic
		__disable_interrupt();
		image_data_buffer_count -= count;
		data_count = image_data_buffer_count;
		__enable_interrupt();

		if (data_count == 0) {
			break;
		}

		count = image_data_buffer[image_data_buffer_tail];
		++image_data_buffer_tail;

		address = image_data_buffer[image_data_buffer_tail];
		address <<= 8;
		++image_data_buffer_tail;
		address |= image_data_buffer[image_data_buffer_tail];
		address <<= 8;
		++image_data_buffer_tail;
		address |= image_data_buffer[image_data_buffer_tail];
		++image_data_buffer_tail;

		// check for a write to the provisioning data
		//   logical:  0x0800 -> 0x0FFF
		//   physical: 0x0000 -> 0x07FF
		if ((address >= MEMORY_OFFSET_PROV_EXT) && (address < (MEMORY_OFFSET_PROV_EXT+MEMORY_LENGTH_PROV))) {
			address -= MEMORY_OFFSET_PROV_EXT;

			eeprom_write(address,&image_data_buffer[image_data_buffer_tail],count);

			image_data_buffer_tail += count;

			if (image_data_buffer_tail >= IMAGE_DATA_BUFFER_SIZE) {
				image_data_buffer_tail = 0;
			}


		} else if ((mmap == MEMORY_MAP_DIRECT) && (address < DISPLAY_EXT_RLE_DATA_ADDRESS_OFFSET)) {
			address -= MEMORY_OFFSET_EXT_NONVOLATILE;

			// uncompressed data
			eeprom_write(address,&image_data_buffer[image_data_buffer_tail],count);
			image_data_buffer_tail += count;

			if (image_data_buffer_tail >= IMAGE_DATA_BUFFER_SIZE) {
				image_data_buffer_tail = 0;
			}

		} else if (((mmap == MEMORY_MAP_DIRECT) && (address >= DISPLAY_EXT_RLE_DATA_ADDRESS_OFFSET)) ||
					((mmap == MEMORY_MAP_RLE) && (address >= DISPLAY_EXT_DATA_ADDRESS_OFFSET))) {

			// RLE data - each byte of RLE data is represented as follows:
			// bit[7] = pixel value (0 or 1)
			// bits[6:0] = #pixels - 1 (so 127 => 128, 0 => 1)
			// Each RLE transfer must start on an 8-pixel boundary (as defined by the address).
			// If the RLE transfer does not end on a byte boundary, the byte is padded
			// with the clear screen value.
			//

			if (mmap == MEMORY_MAP_DIRECT) {
				address -= (DISPLAY_EXT_RLE_DATA_ADDRESS_OFFSET - MEMORY_OFFSET_INT_DISPLAY_DATA + EEPROM_PERSISTENT_MEMORY_OFFSET);
			} else {
				address -= MEMORY_OFFSET_EXT_NONVOLATILE;
			}

			bit = 0x80;
			index = 0;
			byte = 0;
			for (i=0; i<count; ++i) {
				rle_value = image_data_buffer[image_data_buffer_tail];
				++image_data_buffer_tail;
				pixel_count = ((rle_value & 0x7F) + 1);

				while (pixel_count--) {
					if (rle_value & 0x80) {
						byte |= bit;

					} else {
						byte &= ~bit;
					}

					bit >>= 1;
					if (bit == 0) {
						// byte has been completed
						output_buffer[index] = byte;
						++index;
						if (index == 16) {
							eeprom_write(address,output_buffer,16);
							address += 16;
							index = 0;
						}

						bit = 0x80;
						byte = 0;
					}
				}
			}

			// write any remaining data to the memory
			if (index != 0 || bit != 0x80) {
				if (bit != 0x80) {
					output_buffer[index] = byte;
					++index;
				}
				eeprom_write(address,output_buffer,index);
			}

			if (image_data_buffer_tail >= IMAGE_DATA_BUFFER_SIZE) {
				image_data_buffer_tail = 0;
			}
		}

		count += 4;
	}
}



//----------------------------------------------------------------------------
// Called by I2C interrupt
//----------------------------------------------------------------------------
void communications_transfer_start(uint32_t address) {

	transfer_address = address;
	transfer_count = 0;
}



//----------------------------------------------------------------------------
// Called by I2C interrupt
//----------------------------------------------------------------------------
void communications_transfer_update(uint8_t value) {

	// Ensure that the transfer length does not exceede the buffer size.
	// Just drop the extra data.
	if (transfer_count < TRANSFER_BUFFER_SIZE) {
		transfer_data[transfer_count] = value;
		++transfer_count;
	}
}



//----------------------------------------------------------------------------
// Called by I2C interrupt
//----------------------------------------------------------------------------
void communications_transfer_end(void) {
	uint8_t i;
	uint8_t count;
	uint8_t *ptr;
	uint8_t *mask;

	count = transfer_count;
	transfer_count = 0;

	if (count > 0) {
		if (transfer_address < (DISPLAY_EXT_DATA_ADDRESS_OFFSET - MEMORY_OFFSET_EXT_NONVOLATILE)) {
			// Retry filter is only applied to writes to memory.
			previous_transfer_address = 0xFFFFFFFF;

		} else {
			// Check against duplicate messages which might result in unnecessary
			// expenditure of energy
			// ABL and ASAR messages might be delivered multiple times until an ACK
			// is actually received by the transmitting device.
			if (transfer_address == previous_transfer_address) {
				if (count == previous_transfer_count) {
					for (i=0; i<count; ++i) {
						if (transfer_data[i] != previous_transfer_data[i]) {
							break;
						}
					}

					if (i == count) {
						// Found a duplicate of the previous call to communications_transfer_end. Just
						// ignore it.
						return;
					}
				}
			}

			// Now load up the parameters for the next call
			previous_transfer_address = transfer_address;
			previous_transfer_count = count;
			for (i=0; i<count; ++i) {
				previous_transfer_data[i] = transfer_data[i];
			}
		}


		// check for a write to the provisioning area or the screen data area
		if (((transfer_address >= MEMORY_OFFSET_PROV_EXT) && (transfer_address < (MEMORY_OFFSET_PROV_EXT+MEMORY_LENGTH_PROV))) ||
			(transfer_address >= MEMORY_OFFSET_EXT_NONVOLATILE)) {

			// leave the address as-is to reduce confusion
			// it is modified before application

			// ensure the write is in-bounds
			if ((image_data_buffer_count + count + 4) <= IMAGE_DATA_BUFFER_SIZE) {

				image_data_buffer[image_data_buffer_head] = count;
				++image_data_buffer_head;
				image_data_buffer[image_data_buffer_head] = (transfer_address >> 16);
				++image_data_buffer_head;
				image_data_buffer[image_data_buffer_head] = (transfer_address >> 8);
				++image_data_buffer_head;
				image_data_buffer[image_data_buffer_head] = transfer_address;
				++image_data_buffer_head;

				for (i=0; i<count; ++i) {
					image_data_buffer[image_data_buffer_head] = transfer_data[i];
					++image_data_buffer_head;
				}

				if (image_data_buffer_head >= IMAGE_DATA_BUFFER_SIZE) {
					image_data_buffer_head = 0;
				}

				count += 4;
				image_data_buffer_count += count;
			}

		} else {
			if (transfer_address == 0x60C0) {
				// Special case where CC4 event manager writes network time to DD1DO.
				// Re-map it to the correct register and shave off the four unneeded CRC
				// and multicast bytes.
				transfer_address = (REGISTER_NETWORK_TIME * 4);
				count = 4;
				for (i=0; i<4; ++i) {
					transfer_data[i] = transfer_data[i+4];
				}

			} else if (transfer_address == 0x007C) {
				// Special case where clicker writes to tag. Uses old PDI
				// register address.
				transfer_address = (REGISTER_IMMEDIATE_DISPLAY_EVENT * 4);
			}

			ptr = (uint8_t *)registers;
			mask = (uint8_t *)register_read_only_masks;

			for (i=0; i<count; ++i) {
				if (transfer_address >= NUM_REGISTERS * 4) {
					// ignore requests outside range off register space
					break;

				} else {
					// I really don't like doing this
					if (transfer_address == 0x62) {
						if ((transfer_data[i] & 0x01)!=0) {
							eeprom_address_size = 3;
						} else {
							eeprom_address_size = 2;
						}
					}

					ptr[transfer_address] = ((ptr[transfer_address] & mask[transfer_address]) | (transfer_data[i] & ~mask[transfer_address]));
					++transfer_address;
				}
			}
		}
	}
}



//----------------------------------------------------------------------------
// Called by I2C interrupt
//----------------------------------------------------------------------------
/*
	Unused - P Spies 04 19 2017

void communications_set_memory(uint32_t address,uint8_t value) {
}
*/


//----------------------------------------------------------------------------
// Called by I2C interrupt
//----------------------------------------------------------------------------
uint8_t communications_get_memory(uint32_t address) {
	uint8_t *ptr;
	uint8_t buffer = 0;

	// The nonvolatile memory starts at external address MEMORY_OFFSET_EXT_NONVOLATILE and maps to 0x000000
	if (address >= MEMORY_OFFSET_EXT_NONVOLATILE) {
		address -= MEMORY_OFFSET_EXT_NONVOLATILE;

		if (address < eeprom_memory_size) {
			eeprom_read(address,&buffer,1);
		}

	} else if ((address >= MEMORY_OFFSET_PROV_EXT) && (address < (MEMORY_OFFSET_PROV_EXT + MEMORY_LENGTH_PROV))) {
		address -= MEMORY_OFFSET_PROV_EXT;

		if (address < eeprom_memory_size) {
			eeprom_read(address,&buffer,1);
		}


	} else {
		if (address < NUM_REGISTERS * 4) {
			ptr = (uint8_t *)registers;
			buffer = ptr[address];
		}
	}

	// invalidate any previous buffered write
	previous_transfer_count = 0;

	return buffer;
}



//----------------------------------------------------------------------------
// Reads and writes to registers must be atomic
//----------------------------------------------------------------------------
void communications_set_register(uint8_t reg,uint32_t value) {

	if (reg < NUM_REGISTERS) {
		value	= communications_reverse_long(value);
		__disable_interrupt();
		registers[reg] = value;
		__enable_interrupt();
	}
}



//----------------------------------------------------------------------------
// Reads and writes to registers must be atomic
//----------------------------------------------------------------------------
uint32_t communications_get_register(uint8_t reg) {
	uint32_t value = 0;

	if (reg < NUM_REGISTERS) {
		__disable_interrupt();
		value = registers[reg];
		__enable_interrupt();
		value = communications_reverse_long(value);
	}

	return value;
}


#define REV(n) ((n << 24) | (((n>>16)<<24)>>16) | (((n<<16)>>24)<<16) | (n>>24)) 
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#if 0
uint32_t communications_reverse_long(uint32_t value) {
      return REV(value);
}
#else
//----------------------------------------------------------------------------
uint32_t communications_reverse_long(uint32_t value) {
	uint8_t a,b,c,d;

	a = (uint8_t)value;
	value >>= 8;
	b = (uint8_t)value;
	value >>= 8;
	c = (uint8_t)value;
	value >>= 8;
	d = (uint8_t)value;

	value = (uint32_t)a;
	value <<= 8;
	value += (uint32_t)b;
	value <<= 8;
	value += (uint32_t)c;
	value <<= 8;
	value += (uint32_t)d;

	return value;
}
#endif // #if 0


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
/*
	Unused - P Spies 04 19 2017

uint16_t communications_reverse_short(uint16_t value) {
	uint8_t a,b;

	a = (uint8_t)value;
	value >>= 8;
	b = (uint8_t)value;

	value = (uint32_t)a;
	value <<= 8;
	value += (uint32_t)b;

	return value;
}
*/
