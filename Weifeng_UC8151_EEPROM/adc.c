//----------------------------------------------------------------------------
// adc.c
//----------------------------------------------------------------------------
#include "all_includes.h"



//----------------------------------------------------------------------------
//		1. Read reference voltage (1.44 V)
//				ADS:	0x81
//		2. Read temperature with internal sensor
//				ADS:	0x80
//
//		ADM2
//			Vref+ (ADREFP[10])
//				Vdd:	00
//				1.44: 10
//			Vref- (ADREFM)
//				Vss:	 0
//----------------------------------------------------------------------------
void adc_init(void) {

	// enable the ADC clock
	ADCEN = 1U;

	// the applilet code performs this to make sure everything is stopped
	ADM0 = 0x00U;

	// disable the INTAD interrupt
	ADMK = 1U;

	// clear the INTAD interrupt flag
	ADIF = 0U;

	// ???
	ADPC = 0x01U;

	// use a single channel (don't scan), don't trigger scan disable comparator
	// ADCS
	// | ADMD
	// | | FR
	// | | |	 LV
	// | | |	 |	ADCE
	// | | |	 |	|
	// 0_0_010_00_0
	ADM0 = 0x10U;

	// Use single-shot, SW trigger
	//	 FR:010		LV:00		 LowVolt_1		19.00 us
	//
	// ADTMD
	// |	ADCSM
	// |	|		 ADTRS
	// |	|		 |
	// |	|		 |
	// 00_1_000_00
	ADM1 = 0x20U;

	// Vref+: Vdd
	// Vref-: Vss
	// Don't snooze this part
	// Use 10 bit resolution
	//
	// ADREFP
	// |	ADREFM
	// |	|	 ADRCK
	// |	|	 | AWC
	// |	|	 | |	 ADTYP
	// |	|	 | |	 |
	// 00_0_0_0_0_0_0
	ADM2 = 0x00U;

	// Not sure exactly what this is.
	ADUL = 0xFFU;
	ADLL = 0x00U;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void adc_read(uint8_t channel, uint16_t *value, uint8_t samples) {
	uint8_t i;

	if (channel == ADC_CHANNEL_REFERENCE) {
		// select the internal voltage reference
		ADS = 0x81U;
	} else {
		// select the internal temp sensor
		ADS = 0x80U;
	}

	timer_delay(1);

	// turn on the comparator
	ADCE = 1U;

	*value = 0;
	
	// The first conversion is invalid, so increment samples
	samples++;

	for (i=0; i<samples; i++) {
		// delay about 1 us
		timer_delay(1);

		// start the conversion
		ADCS = 1;

		// Wait for the conversion to complete
		while (ADIF == 0);

		// clear the interrupt flag
		ADIF=0;

		// skip the first conversion
		if (i != 0) {
			*value += (uint16_t)(ADCR >> 6U);
		}
	}

	ADCE = 0U;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void adc_shutdown(void) {

	ADS = 0U;
	ADCE = 0U;
	ADCEN = 0U;
}
