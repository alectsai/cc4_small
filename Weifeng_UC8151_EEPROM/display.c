//----------------------------------------------------------------------------
// display.c
//----------------------------------------------------------------------------
#include "all_includes.h"
#include "memory.h"
#include "provisioning.h"
#include "crc32.h"
#include "uc.h"

#define TIMEOUT_POWER_UP	16	// 4 seconds
#define TIMEOUT_RENDER		240	// 60 seconds

#define DISPLAY_BUFFER_SIZE 4

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
uint8_t display_size;
uint8_t display_color;
//uint8_t display_configuration;	//dual_display_mode
uint8_t display_num_screens;
uint32_t display_time_since_last_render;
uint8_t display_render_complete;

//
// screens should align along block boundaries so that they can be cleared
// without affecting adjacent screens
//
const uint16_t display_screen_bytes[4] = {
	0x0B48,	// page boundary for 0x1388 1.54" glass		152x152
	0x0AC4,	// page boundary for 0xAC4 for 2.13" glasss 	104x212
	0x1280,	// 2.9"	4736					128x296
	0x3A98	// 4.2" 15000					400x300
};

//----------------------------------------------------------------------------
void display_sleep(void)
{
	uint8_t	cmd_buf[2];

	cmd_buf[0] = UC_CMD_07_DSLP;
	cmd_buf[1] = 0xA5;
	DISPLAY_DC = 0;
	display_spi_transfer(cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);
}

//----------------------------------------------------------------------------
uint8_t get_display_status(void)
{
	uint8_t	cmd_buf[1];

	cmd_buf[0] = UC_CMD_71_FLG;
	DISPLAY_DC = 0;
	display_spi_transfer(cmd_buf,1,SPI_TRANSFER_CONTINUATION);
	display_spi_read(cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);
	return(cmd_buf[0]);
}

//----------------------------------------------------------------------------
void display_initialize(void) {

	//dual_display_mode = SINGLE_DISPLAY;
	//display_size = DISPLAY_SIZE_1p54;
	//display_color = DISPLAY_COLOR_BW;
	//display_configuration = DISPLAY_CONFIGURATION_ONE_DISPLAY;

	// this is enough to get things going, it might be updated later
	eeprom_memory_size = (32L * 1024L);
	eeprom_page_size = 64;
}

void display_configure(uint16_t type) {
	uint32_t value;

	display_color = DISPLAY_COLOR_BW;

	switch (type) {
		case 0xD0:
		case 0xF5:
			display_size = DISPLAY_SIZE_1p54;
			break;
		case 0xB0:
			display_size = DISPLAY_SIZE_1p54;
			display_color = DISPLAY_COLOR_BWR;
			break;
		case 0xA0:
			display_size = DISPLAY_SIZE_1p54;
			display_color = DISPLAY_COLOR_BWY;
			break;

		case 0xD1:
		case 0xF6:
			display_size = DISPLAY_SIZE_2p13;
			break;
		case 0xB1:
			display_size = DISPLAY_SIZE_2p13;
			display_color = DISPLAY_COLOR_BWR;
			break;
		case 0xA1:
			display_size = DISPLAY_SIZE_2p13;
			display_color = DISPLAY_COLOR_BWY;
			break;

		case 0xD2:
			display_size = DISPLAY_SIZE_2p90;
			break;
		case 0xB2:
			display_size = DISPLAY_SIZE_2p90;
			display_color = DISPLAY_COLOR_BWR;
			break;
		case 0xA2:
			display_size = DISPLAY_SIZE_2p90;
			display_color = DISPLAY_COLOR_BWY;
			break;

		case 0xD4:
		case 0xF4:
			display_size = DISPLAY_SIZE_4p20;
			break;
		case 0xB4:
			display_size = DISPLAY_SIZE_4p20;
			display_color = DISPLAY_COLOR_BWR;
			break;
		case 0xA4:
			display_size = DISPLAY_SIZE_4p20;
			display_color = DISPLAY_COLOR_BWY;
			break;
		default:
			error_report(ERROR_INVALID_TYPE);
	}

	if( display_size == DISPLAY_SIZE_4p20 )
	{
		// 1Mbit, 256 byte page size - 8 screens
		eeprom_memory_size = (128L * 1024L);
		eeprom_page_size = 256;
		eeprom_address_size = 3;
	}

	display_num_screens = (uint8_t)(eeprom_memory_size / display_screen_bytes[display_size]);

	// if it's not BW, it's either BWR of BWY -> half the screens
	if (display_color != DISPLAY_COLOR_BW) {
		display_num_screens >>= 1;
	}

	if (display_num_screens > 10) {
		// limit to 10 screens for now
		display_num_screens = 10;
	}

	//value = ((uint32_t)display_configuration << 12) | ((uint32_t)display_color << 8) | (uint32_t)display_size; //dual_display_mode
	value = ((uint32_t)display_color << 8) | (uint32_t)display_size;
	communications_set_register(REGISTER_DISPLAY_TYPE,value);

	// Take it out of reset for deep sleep command

	//display_power_on_reset();

	//display_sleep();
}

int display_wait(uint8_t duration) {
    // Wait for BUSY bit to go high
    uint8_t k = timer_increment;
    while (DISPLAY_BUSY_0 == 0) {
        // Save some power on the MCU. Let the timer wake it up every quarter second.
        __halt();
        WDTE = 0xAC; // Service the WDT
        if ((timer_increment - k) >= duration) {
            return 1;
	}
    }
    return 0;
}


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void display_render(uint8_t screen) {
	int32_t j;
//	uint8_t k;  //alec
	uint8_t cmd_buf[8];
	uint8_t image_in_data[DISPLAY_BUFFER_SIZE];
	uint32_t address;
	uint32_t addressC;
	//uint32_t address2;	//dual_display_mode
	//uint32_t address2C;	//dual_display_mode
	uint32_t size;
	uint32_t size_image;
	uint32_t render_count;
	uint8_t retry;
	uint32_t error;
	uint32_t register_0180;
	uint32_t register_0184;
	uint8_t current_voltage_reading;

	//uint8_t lpd_A, lpd_B;	//dual_display_mode

	uint8_t lpd_A = 1;	// "Normal" if we don't actually do the render

	current_voltage_reading = 0xFF;

	error = 0;

	if (screen >= display_num_screens) {
		return;
	}

	// Set "Rendering" status
	communications_set_register( REGISTER_STATUS, (communications_get_register(REGISTER_STATUS)&STATUS_RENDER_STATE_MASK) | STATUS_RENDER_STATE_RENDERING );

	// Power on the EEPROM
	eeprom_power(EEPROM_POWER_ON);

	for (retry = 0; retry < 3; ++retry) {  // was 3, alec

		// First pass, use 1-second reset
		// Second pass, use 2-second reset
		// Third pass, use triple-pulse reset

		if( retry < 2 )
		{
			DISPLAY_RESET = 1;

			timer_delay( 1 );

			DISPLAY_RESET = 0;

			WDTE = 0xAC; 		// Service the WDT

			timer_delay( LONG_RESET_MILLISECONDS + (LONG_RESET_MILLISECONDS*retry) );

			DISPLAY_RESET = 1;

			timer_delay( 1 );
		}
		else
		{
			display_power_on_reset();
		}

		error = 0;

		// BTST - send to both dislays
		cmd_buf[0] = UC_CMD_06_BTST;
		//
		cmd_buf[1] = 0x17;
		cmd_buf[2] = 0x17;
		cmd_buf[3] = 0x17;
		DISPLAY_DC = 0;
		//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,4,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
		display_spi_transfer(cmd_buf,4,SPI_TRANSFER_NO_CONTINUATION);

		// PON
		cmd_buf[0] = UC_CMD_04_PON;
		DISPLAY_DC = 0;
		//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
		display_spi_transfer(cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);

		// Check for BUSY low

		if( DISPLAY_BUSY_0 == 0 )
		{
			// Wait for BUSY bit to go high
#if 0  
			k = timer_increment;
			while (DISPLAY_BUSY_0 == 0) {
				// Save some power on the MCU. Let the timer wake it up every quarter second.
				__halt();
				WDTE = 0xAC; // Service the WDT

				// maximum 2 second wait for power up
				if ((timer_increment - k) >= TIMEOUT_POWER_UP) {
					error |= ERROR_RENDER_TIMEOUT_POWERON;
					break;
				}
			}
#else
                        if(display_wait(TIMEOUT_POWER_UP)) {
                            error |= ERROR_RENDER_TIMEOUT_POWERON;
                        }
#endif
			/* if (dual_display_mode == DUAL_DISPLAY) {
				while (DISPLAY_BUSY_1 == 0) {
					// Save some power on the MCU. Let the timer wake it up every quarter second.
					__halt();
					WDTE = 0xAC; // Service the WDT
	
					// maximum 60 second wait for render commands
					if ((timer_increment - k) >= TIMEOUT_POWER_UP) {
						error |= ERROR_RENDER_TIMEOUT_POWERON;
						break;
					}
				}
			} */
		}
		else // From check for BUSY low
		{
			error = ERROR_RENDER_TIMEOUT_POWERON;
		}

		if (error == 0x00000000) {
			// PSR - send to both dislays
			cmd_buf[0] = UC_CMD_00_PSR;
			if (display_color == DISPLAY_COLOR_BW) {
				cmd_buf[1] = 0xDF;
			} else {
				cmd_buf[1] = 0x0F;
			}
			// Shutdown configuration:
			//   0x0B: use the NORG configuration at shutdown
			//   0x0D: use the TIEG configuration at shutdown
			cmd_buf[2] = 0x0D;
			DISPLAY_DC = 0;
			//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,3,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
			display_spi_transfer(cmd_buf,3,SPI_TRANSFER_NO_CONTINUATION);

			// TRES - send to both dislays
			//   HRES[7:3],0b000
			//	 0b0000000,VRES[8]
			//   HREF[7:0]
			//
			//	for 1.54" glass: 152x152	0x98, 0x00, 0x98
			//	for 2.13" glass: 104x212	0x68, 0x00, 0xD4
			//	for 2.9"  glass: 128x296	0x80, 0x01, 0x28
			//	for 4.2"  glass: 400x300	0x80, 0x01, 0x28
			//
			//
			// using i to index the bytes because the 4.2" display uses 4 bytes of data
			// as opposed the the 3 of the other sizes
			j=0;
			cmd_buf[j++] = UC_CMD_61_TRES;
			switch (display_size) {
				case DISPLAY_SIZE_1p54:
					cmd_buf[j++] = 0x98;
					cmd_buf[j++] = 0x00;
					cmd_buf[j++] = 0x98;
					break;
				case DISPLAY_SIZE_2p13:
					cmd_buf[j++] = 0x68;
					cmd_buf[j++] = 0x00;
					cmd_buf[j++] = 0xD4;
					break;
				case DISPLAY_SIZE_2p90:
					cmd_buf[j++] = 0x80;
					cmd_buf[j++] = 0x01;
					cmd_buf[j++] = 0x28;
					break;
				case DISPLAY_SIZE_4p20:
					cmd_buf[j++] = 0x01;
					cmd_buf[j++] = 0x90;
					cmd_buf[j++] = 0x01;
					cmd_buf[j++] = 0x2C;
					break;
			}
			
			DISPLAY_DC = 0;
			//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,j,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
			display_spi_transfer(cmd_buf,j,SPI_TRANSFER_NO_CONTINUATION);

			// CDI - send to both dislays
			cmd_buf[0] = UC_CMD_50_CDI;
			if (display_color == DISPLAY_COLOR_BW) {
				cmd_buf[1] = 0x97;
			} else {
				cmd_buf[1] = 0x77;
			}
			DISPLAY_DC = 0;
			//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
			display_spi_transfer(cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);


			// Send the BW data to the first screen
			if (display_color == DISPLAY_COLOR_BW) {
				cmd_buf[0] = UC_CMD_13_DTM2;
			} else {
				cmd_buf[0] = UC_CMD_10_DTM1;
			}
			DISPLAY_DC = 0;
			//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
			display_spi_transfer(cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);

			size = (uint32_t) (display_screen_bytes[display_size]);
			size_image = (uint32_t) (display_screen_bytes[display_size]);

			if (display_color == DISPLAY_COLOR_BW) {
				address = ((uint32_t) screen) * size;

				/* if (dual_display_mode == DUAL_DISPLAY) {
					address <<= 1;
					address2 = address + size + EEPROM_PERSISTENT_MEMORY_OFFSET;
				} */

			} else {
				// we've got a color display
				address = ((uint32_t) screen) * (size << 1);
				addressC = address + size;

				/* if (dual_display_mode == DUAL_DISPLAY) {
					address <<= 1;
					addressC = address + size;
					address2 = addressC + size + EEPROM_PERSISTENT_MEMORY_OFFSET;
					address2C = address2 + size + EEPROM_PERSISTENT_MEMORY_OFFSET;
				} */

				addressC += EEPROM_PERSISTENT_MEMORY_OFFSET;
			}

			address += EEPROM_PERSISTENT_MEMORY_OFFSET;

			DISPLAY_DC = 1;
 			while (size_image) {
				// no conversion needed
				eeprom_read(address,image_in_data,DISPLAY_BUFFER_SIZE);
				address += DISPLAY_BUFFER_SIZE;
				for (j=0; j<DISPLAY_BUFFER_SIZE; ++j) {
					image_in_data[j] ^= 0xFF;
				}
				//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_0,image_in_data,DISPLAY_BUFFER_SIZE,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
				display_spi_transfer(image_in_data,DISPLAY_BUFFER_SIZE,SPI_TRANSFER_NO_CONTINUATION);
				size_image -= DISPLAY_BUFFER_SIZE;
			}
/*
			if (dual_display_mode == DUAL_DISPLAY) {

				// address will still be valid
				size_image = (uint32_t) display_screen_bytes[display_size];

			 	while (size_image) {
					// no conversion needed
					eeprom_read(address2,image_in_data,DISPLAY_BUFFER_SIZE);
					address2 += DISPLAY_BUFFER_SIZE;
					for (j=0; j<DISPLAY_BUFFER_SIZE; ++j) {
						image_in_data[j] ^= 0xFF;
					}
					display_spi_transfer(DISPLAY_SPI_DEVICE_CS_1,image_in_data,DISPLAY_BUFFER_SIZE,SPI_TRANSFER_NO_CONTINUATION);
					size_image -= DISPLAY_BUFFER_SIZE;
				}
			}
*/
			// if the display is color (BWR, BWY, or BRWY)
			if (display_color != DISPLAY_COLOR_BW) {
				cmd_buf[0] = UC_CMD_13_DTM2;
				DISPLAY_DC = 0;
				//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
				display_spi_transfer(cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);

				DISPLAY_DC = 1;
				size_image = (uint32_t) display_screen_bytes[display_size];
	 			while (size_image) {
					// no conversion needed
					eeprom_read(addressC,image_in_data,DISPLAY_BUFFER_SIZE);
					addressC += DISPLAY_BUFFER_SIZE;
					for (j=0; j<DISPLAY_BUFFER_SIZE; ++j) {
						image_in_data[j] ^= 0xFF;
					}
					//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_0,image_in_data,DISPLAY_BUFFER_SIZE,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
					display_spi_transfer(image_in_data,DISPLAY_BUFFER_SIZE,SPI_TRANSFER_NO_CONTINUATION);
					size_image -= DISPLAY_BUFFER_SIZE;
				}

	
				/* if (dual_display_mode == DUAL_DISPLAY) {
					// address will still be valid
					size_image = (uint32_t) display_screen_bytes[display_size];
	
				 	while (size_image) {
						// no conversion needed
						eeprom_read(address2C,image_in_data,DISPLAY_BUFFER_SIZE);
						address2C += DISPLAY_BUFFER_SIZE;
						for (j=0; j<DISPLAY_BUFFER_SIZE; ++j) {
							image_in_data[j] ^= 0xFF;
						}
						display_spi_transfer(DISPLAY_SPI_DEVICE_CS_1,image_in_data,DISPLAY_BUFFER_SIZE,SPI_TRANSFER_NO_CONTINUATION);
						size_image -= DISPLAY_BUFFER_SIZE;
					}
				} */
			}

			// DRF - send to both dislays
			// NOTE: This will cause 2x peak and high current draw from the batteries.
			// 		 The battery pack will need to be sized accordingly.
			cmd_buf[0] = UC_CMD_12_DRF;
			DISPLAY_DC = 0;
			//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
			display_spi_transfer(cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);

			// wait 1 ms for the uc8151 to start processing
			timer_delay(1);

			// trigger a read of the battery voltage
			if (i2c_master_read_register(0x0180,&register_0180) == I2C_M_ACK) {
				if (i2c_master_write_register(0x0180,(register_0180|0x00000400)) == I2C_M_ACK) {
					i2c_master_write_register(0x0180,(register_0180|0x00040000));
				}
			}

			//
			// Wait for BUSY bit to go high after the render
#if 0          
			k = timer_increment;
			while (DISPLAY_BUSY_0 == 0) {
				// Save some power on the MCU. Let the timer wake it up every quarter second.
				__halt();
				WDTE = 0xAC; // Service the WDT
	 				 
				// maximum 60 second wait for render commands
				if ((timer_increment - k) >= TIMEOUT_RENDER) {
					error |= ERROR_RENDER_TIMEOUT_RENDER;
					break;
				}
			}
#else
                        if(display_wait(TIMEOUT_RENDER)) {
                            error |= ERROR_RENDER_TIMEOUT_RENDER;
                        }
#endif
                     //error |= ERROR_RENDER_TIMEOUT_RENDER;  // testing, alec

			/* if (dual_display_mode == DUAL_DISPLAY) {
				while (DISPLAY_BUSY_1 == 0) {
					// Save some power on the MCU. Let the timer wake it up every quarter second.
					__halt();
					WDTE = 0xAC; // Service the WDT
		 				 
					// maximum 60 second wait for render commands
					if ((timer_increment - k) >= TIMEOUT_RENDER) {
						error |= ERROR_RENDER_TIMEOUT_RENDER;
						break;
					}
				}
			} */

			// disable automatic sampling of the battery voltage
			i2c_master_write_register(0x0180,(register_0180|0x00000400));

			// read the battery voltage
			if (i2c_master_read_register(0x0184,&register_0184) == I2C_M_ACK) {
				//
				register_0184 = (register_0184 >> 16)&0x0FF;
				if (register_0184 < current_voltage_reading) {
					current_voltage_reading = register_0184;
				}
			}

			/* if( dual_display_mode == DUAL_DISPLAY )
			{
				cmd_buf[0] = UC_CMD_51_LPD;
				DISPLAY_DC = 0;
				display_spi_transfer(DISPLAY_SPI_DEVICE_CS_1,cmd_buf,1,SPI_TRANSFER_CONTINUATION);
				display_spi_read(DISPLAY_SPI_DEVICE_CS_1,&lpd_B,1,SPI_TRANSFER_NO_CONTINUATION);
			}
			else
			{
				lpd_B = 1;	// "Normal" status for error check below
			} */

			//
			// Before we power-off, WF recommends a power-down sequences
			//
			// CDI - send to both dislays
			cmd_buf[0] = UC_CMD_50_CDI;
			cmd_buf[1] = 0xD7;
			DISPLAY_DC = 0;
			//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
			display_spi_transfer(cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);
#ifdef ENABLE_VDCS_PWR
			// VDCS - send to both dislays
			cmd_buf[0] = UC_CMD_82_VDCS;
			cmd_buf[1] = 0x00;
			DISPLAY_DC = 0;
			//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
			display_spi_transfer(cmd_buf,2,SPI_TRANSFER_NO_CONTINUATION);

			// delay - wait for VCOM to go to 0V
			// 10 ms
			timer_delay(10);

			// PWR - send to both dislays
			cmd_buf[0] = UC_CMD_01_PWR;
			cmd_buf[1] = 0x00;
			cmd_buf[2] = 0x00;
			cmd_buf[3] = 0x00;
			cmd_buf[4] = 0x00;
			cmd_buf[5] = 0x00;
			DISPLAY_DC = 0;
			//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,6,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
			display_spi_transfer(cmd_buf,6,SPI_TRANSFER_NO_CONTINUATION);

			// delay - wait for VGH, VGL, VSH, VSL to go to 0V
			// 10 ms
			timer_delay(10);
#endif
		} // End of "if (error == 0x00000000)" from power-on timeout

		communications_set_register(REGISTER_DEBUG,(uint32_t)get_display_status());

		// read the LPD bit for each display
		cmd_buf[0] = UC_CMD_51_LPD;
		DISPLAY_DC = 0;
		//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_0,cmd_buf,1,SPI_TRANSFER_CONTINUATION); //dual_display_mode
		display_spi_transfer(cmd_buf,1,SPI_TRANSFER_CONTINUATION);
		//display_spi_read(DISPLAY_SPI_DEVICE_CS_0,&lpd_A,1,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
		display_spi_read(&lpd_A,1,SPI_TRANSFER_NO_CONTINUATION);

		// POF - send to both dislays
		cmd_buf[0] = UC_CMD_02_POF;
		DISPLAY_DC = 0;
		//display_spi_transfer(DISPLAY_SPI_DEVICE_CS_ALL,cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION); //dual_display_mode
		display_spi_transfer(cmd_buf,1,SPI_TRANSFER_NO_CONTINUATION);

		// Display status update delay
		timer_delay(50);

              if(display_wait(12)) {  // 3 seconds
                    error |= ERROR_RENDER_TIMEOUT_POWEROFF;
              }

              //error |= ERROR_RENDER_TIMEOUT_POWEROFF;  //alec, testing fail render count is okay

		if (error == 0) {
			break;
		}

	} // End of retry loop

	// Borrow "render_count" for render status bits
	render_count = communications_get_register(REGISTER_STATUS) & STATUS_RENDER_STATE_MASK;

	// Use "register_0184" for render fail count (bits 20:13)
	register_0184 = (render_count>>STATUS_RENDER_FAIL_COUNT_SHIFT) & 0xFF;

	// Post status if timeout during wait was necessary
	if (error != 0) {
		// Setup "Render Fail" bits
		render_count |= ( error | STATUS_RENDER_STATE_RENDER_FAIL );

		if( register_0184 < 255 )	// Saturate at 255
		{
			// Bump render fail count for EEPROM write below
			cmd_buf[0] = (uint8_t)++register_0184;
		}
	}
	else
	{
		// Setup "Render Pass" bits
		render_count |= STATUS_RENDER_STATE_RENDER_PASS;
	}

	render_count = (render_count&STATUS_RENDER_FAIL_COUNT_MASK) | (register_0184<<STATUS_RENDER_FAIL_COUNT_SHIFT);

	communications_set_register( REGISTER_STATUS, render_count );

	// Physically turn off the display
	timer_delay(10);
	DISPLAY_SPI_MOSI = 0;
	DISPLAY_SPI_CLK = 0;

#define DEEP_SLEEP
#ifdef DEEP_SLEEP

	display_sleep();

	DISPLAY_DC = 0;
	DISPLAY_SPI_CS_0 = 0;
	//DISPLAY_SPI_CS_1 = 0; //dual_display_mode

#else
	POWER_DISPLAY_ENA = 1;
	DISPLAY_SPI_CS_0 = 0;
	//DISPLAY_SPI_CS_1 = 0; //dual_display_mode

	DISPLAY_DC = 0;
	DISPLAY_RESET = 0;

#endif // DEEP_SLEEP

	// Update render count
	render_count = communications_get_register(REGISTER_RENDER_COUNT);
	++render_count;
	communications_set_register(REGISTER_RENDER_COUNT,render_count);

	// Save it in EEPROM
	eeprom_write_uint32( PROVISIONING_RENDER_COUNT_OFFSET, render_count );

	// Update render voltage register
	// reuse register_0180
	register_0180 = communications_get_register(REGISTER_RENDER_VOLTAGE);
	//if ((lpd_A & lpd_B) == 0) { //dual_display_mode
	if( lpd_A == 0 ) {
		// set both bits
		register_0180 |= 0xC0000000;
	} else {
		// clear the current bit, leave the sticky bit alone
		register_0180 &= 0xBFFFFFFF;
	}

	if (error != 0) {
		// set both bits
		register_0180 |= 0x30000000;
	} else {
		// clear the current bit, leave the sticky bit alone
		register_0180 &= 0xEFFFFFFF;
	}

	register_0180 &= 0xFFFFFF00;
	register_0180 |= current_voltage_reading;

	if (((register_0180>>8)&0x0FF) > current_voltage_reading) {
		register_0180 &= 0xFFFF00FF;
		register_0180 |= ((current_voltage_reading<<8)&0x0000FF00);
	}

	communications_set_register(REGISTER_RENDER_VOLTAGE,register_0180);

	// save the screen number, render voltage, and render fail count in EEPROM
	cmd_buf[0] = screen;
	cmd_buf[1] = current_voltage_reading;
	cmd_buf[2] = (uint8_t)register_0184;	// Render fail count
	eeprom_write(PROVISIONING_LAST_RENDER_SCREEN_OFFSET, cmd_buf, 3);

	// this may or may not be safe, commenting out for now...
	//i2c_master_write_register(0x0500,register_0180);

	// Update time since last render.
	display_render_complete = 1;

}

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void display_clear_screen(uint8_t screen) {
	uint32_t address;
	uint16_t bytes;

	if (screen < display_num_screens) {

		// all screen boundaries are aligned to 256 bytes
		// all length are aligned to 128 bytes at worst

		bytes = display_screen_bytes[display_size];
		// if it's not BW, it's either BWR of BWY -> 2x bytes required
		if (display_color != DISPLAY_COLOR_BW) {
			bytes <<= 1;
		}
		address = (uint32_t)screen * (uint32_t)bytes;

		while (bytes >= eeprom_page_size) {
			// the CAT25256 EEPROM does not have page/sector clear commands, clear byte-by-byte
			// on a page basis

			// erase the screen one page at a time
			eeprom_write(address+EEPROM_PERSISTENT_MEMORY_OFFSET,DISPLAY_CLEAR_SCREEN_WHITE_VALUE,eeprom_page_size);
			address += eeprom_page_size;
			bytes -= eeprom_page_size;
		}

		if (bytes != 0) {
			// if there is a fraction of a page left over, erase it too
			eeprom_write(address+EEPROM_PERSISTENT_MEMORY_OFFSET,DISPLAY_CLEAR_SCREEN_WHITE_VALUE,(uint8_t)bytes);
		}
	}
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//void display_spi_transfer(uint8_t device, uint8_t *buffer_out, uint16_t size, uint8_t continuation) { //dual_display_mode
void display_spi_transfer(uint8_t *buffer_out, uint16_t size, uint8_t continuation) {
	uint16_t i;

	// assert chip select

	DISPLAY_SPI_CS_0 = 0;

/* //dual_display_mode
	if (device & DISPLAY_SPI_DEVICE_CS_0) {
		DISPLAY_SPI_CS_0 = 0;
	}

	if( (dual_display_mode == DUAL_DISPLAY) && (device & DISPLAY_SPI_DEVICE_CS_1) )
	{
		DISPLAY_SPI_CS_1 = 0;
	}
*/
	// perform transfer one byte at a time
	for (i=0; i<size; ++i) {
		display_shift_one_byte(buffer_out[i]);
		DISPLAY_DC = 1;
	}

	// de-assert chip select
	if (continuation == SPI_TRANSFER_NO_CONTINUATION) {
		DISPLAY_SPI_CS_0 = 1;
		//DISPLAY_SPI_CS_1 = 1; //dual_display_mode
	}

	// Service the WDT
	WDTE = 0xAC;
}


//----------------------------------------------------------------------------
// according to the IAR c compiler manual Register A will alway be used for the 1st parameter
// so we can assume byte2Shift will be in register A
//----------------------------------------------------------------------------
void display_shift_one_byte(uint8_t byte) {
    asm("MOV1 CY, A.7");
    asm("MOV1 S:P0.2, CY");
    asm("SET1 S:P0.0");
    asm("CLR1 S:P0.0");
    
    asm("MOV1 CY,A.6");
    asm("MOV1 S:P0.2, CY");
    asm("SET1 S:P0.0");
    asm("CLR1 S:P0.0");

    asm("MOV1 CY,A.5");
    asm("MOV1 S:P0.2, CY");
    asm("SET1 S:P0.0");
    asm("CLR1 S:P0.0");
    
    asm("MOV1 CY,A.4");
    asm("MOV1 S:P0.2, CY");
    asm("SET1 S:P0.0");
    asm("CLR1 S:P0.0");
    
    asm("MOV1 CY,A.3");
    asm("MOV1 S:P0.2, CY");
    asm("SET1 S:P0.0");
    asm("CLR1 S:P0.0");
    
    asm("MOV1 CY,A.2");
    asm("MOV1 S:P0.2, CY");
    asm("SET1 S:P0.0");
    asm("CLR1 S:P0.0");
    
    asm("MOV1 CY,A.1");
    asm("MOV1 S:P0.2, CY");
    asm("SET1 S:P0.0");
    asm("CLR1 S:P0.0");
    
    asm("MOV1 CY,A.0");
    asm("MOV1 S:P0.2, CY");
    asm("SET1 S:P0.0");
    asm("CLR1 S:P0.0");
}

//-----------------------------------------------------------------------------
// Power on both display and reset them.
// Also put both devices select to disable
//-----------------------------------------------------------------------------
void display_power_on_reset( void ) {
	// Set I/O pins to proper state
	DISPLAY_RESET = 1;
	DISPLAY_SPI_CS_0 = 1;
	//DISPLAY_SPI_CS_1 = 1; //dual_display_mode

	// Power on display
	POWER_DISPLAY_ENA = 0;

	// Reset display 3 times per Weifeng
	// "UC8151_fail to awake from deep sleep.pdf" dated 2016-03-04

	busyWait( WEIFENG_RESET_PULSE_WIDTH );	// Min 50uS, max 200uS
	DISPLAY_RESET = 0;
	busyWait( WEIFENG_RESET_PULSE_WIDTH );	// Min 50uS, max 200uS
	DISPLAY_RESET = 1;
	busyWait( WEIFENG_RESET_PULSE_WIDTH );	// Min 50uS, max 200uS
	DISPLAY_RESET = 0;
	busyWait( WEIFENG_RESET_PULSE_WIDTH );	// Min 50uS, max 200uS
	DISPLAY_RESET = 1;
	busyWait( WEIFENG_RESET_PULSE_WIDTH );	// Min 50uS, max 200uS
	DISPLAY_RESET = 0;
	busyWait( WEIFENG_RESET_PULSE_WIDTH );	// Min 50uS, max 200uS
	DISPLAY_RESET = 1;

	timer_delay(1);
}


//void display_spi_read(uint8_t device, uint8_t *buffer_in, uint16_t size, uint8_t continuation) { //dual_display_mode
void display_spi_read(uint8_t *buffer_in, uint16_t size, uint8_t continuation) {
	uint16_t i;
	uint8_t bit;
	uint8_t data;

	// assert chip select, only one at a time for reading...

	DISPLAY_SPI_CS_0 = 0;
/* //dual_display_mode
	if (device & DISPLAY_SPI_DEVICE_CS_0) {
		DISPLAY_SPI_CS_0 = 0;
	} else if (device & DISPLAY_SPI_DEVICE_CS_1) {
		DISPLAY_SPI_CS_1 = 0;
	}
*/

	// change the MOSI pin to an input
	DISPLAY_SPI_DIR_MOSI = DISPLAY_SPI_INPUT;

	// perform transfer one byte at a time
	for (i=0; i<size; ++i) {
		data = 0;
		for (bit = 0x80; bit != 0; bit >>= 1) {
			DISPLAY_SPI_CLK = 1;
			data = (data<<1) | DISPLAY_SPI_MOSI;
			DISPLAY_SPI_CLK = 0;
		}
		buffer_in[i] = data;
	}

	// change the MOSI pin to back an output
	DISPLAY_SPI_DIR_MOSI = DISPLAY_SPI_OUTPUT;

	// de-assert chip selects
	if (continuation == SPI_TRANSFER_NO_CONTINUATION) {
		DISPLAY_SPI_CS_0 = 1;
		//DISPLAY_SPI_CS_1 = 1; //dual_display_mode
	}

	// Service the WDT
	WDTE = 0xAC;
}



void display_copy_screen(uint32_t cmd) {
	uint32_t address_src_A;
	//uint32_t address_src_B; //dual_display_mode
	uint32_t address_dst_A;
	//uint32_t address_dst_B; //dual_display_mode
	uint32_t size;
	uint32_t num_bytes;
	uint32_t size_image;
	uint32_t size_image_total;
	uint16_t i;
	uint8_t image_data[DISPLAY_BUFFER_SIZE];
	uint8_t image_data_2[DISPLAY_BUFFER_SIZE];

	uint32_t src_screen = (cmd >> 16) & 0x0000000F;
	uint32_t dst_screen = (cmd >> 8) & 0x0000000F;

	if ((src_screen < display_num_screens) && (dst_screen < display_num_screens)) {
		// Power on the EEPROM
		eeprom_power(EEPROM_POWER_ON);

		// the number of bytes for each screen including unused space
		size = (uint32_t) (display_screen_bytes[display_size]);

		// the number of bytes for each screen image data only
		size_image = (uint32_t)(display_screen_bytes[display_size]);
		size_image_total = size_image;


		if (display_color == DISPLAY_COLOR_BW) {
			//if (dual_display_mode == DUAL_DISPLAY) {
			//	address_src_A = src_screen * (size << 1);
			//	address_dst_A = dst_screen * (size << 1);
			//	size_image_total <<= 1;
			//} else {
				address_src_A = src_screen * size;
				address_dst_A = dst_screen * size;
			//}
		} else {
			//if (dual_display_mode == DUAL_DISPLAY) {
			//	address_src_A = src_screen * (size << 2);
			//	address_dst_A = dst_screen * (size << 2);
			//	size_image_total <<= 2;
			//} else {
				address_src_A = src_screen * (size << 1);
				address_dst_A = dst_screen * (size << 1);
				size_image_total <<= 1;
			//}
		}

		address_src_A += DISPLAY_INT_DATA_ADDRESS_OFFSET;
		//address_src_B = address_src_A + size; //dual_display_mode
		address_dst_A += DISPLAY_INT_DATA_ADDRESS_OFFSET;
		//address_dst_B = address_dst_A + size; //dual_display_mode

		size_image = size_image_total;
		while (size_image) {
			num_bytes = size_image;
			if (num_bytes > DISPLAY_BUFFER_SIZE) {
				num_bytes = DISPLAY_BUFFER_SIZE;
			}

			eeprom_read(address_src_A,image_data,num_bytes);

			if ((cmd&0x00000002) != 0) {
				eeprom_read(address_dst_A,image_data_2,num_bytes);
				for(i=0;i<DISPLAY_BUFFER_SIZE;i++) {
					image_data[i] = image_data[i] | image_data_2[i];
				}
			}

			eeprom_write(address_dst_A,image_data,num_bytes);
			address_src_A += num_bytes;
			address_dst_A += num_bytes;
			size_image -= num_bytes;

			// Service the WDT
			WDTE = 0xAC;
		}

/*
		if (dual_display_mode == DUAL_DISPLAY) {
			// address will still be valid
			size_image = size_image_total;
			while (size_image) {
				num_bytes = size_image;
				if (num_bytes > DISPLAY_BUFFER_SIZE) {
					num_bytes = DISPLAY_BUFFER_SIZE;
				}

				eeprom_read(address_src_B,image_data,num_bytes);

				if ((cmd&0x00000002) != 0) {
					eeprom_read(address_dst_B,image_data_2,num_bytes);
					for(i=0;i<DISPLAY_BUFFER_SIZE;i++) {
						image_data[i] = image_data[i] | image_data_2[i];
					}
				}

				eeprom_write(address_dst_B,image_data,num_bytes);
				address_src_B += num_bytes;
				address_dst_B += num_bytes;
				size_image -= num_bytes;

				// Service the WDT
				WDTE = 0xAC;
			}
		}
*/
	}
}




void display_crc(uint32_t cmd) {
	uint32_t screen;
	uint32_t crc;
	uint8_t i;
	uint32_t num_bytes;
	uint32_t address_A;
	//uint32_t address_B; //dual_display_mode
	uint32_t size;
	uint32_t size_image;
	uint32_t size_image_total;
	uint8_t image_data[DISPLAY_BUFFER_SIZE];


	crc32_init(&crc);

	// Power on the EEPROM
	eeprom_power(EEPROM_POWER_ON);

	for (screen = 0; screen < display_num_screens; screen++) {
		if (cmd & (0x100U << screen)) {

			// the number of bytes for each screen including unused space
			size = (uint32_t) (display_screen_bytes[display_size]);

			// the number of bytes for each screen image data only
			size_image = (uint32_t) (display_screen_bytes[display_size]);
			size_image_total = size_image;

			if (display_color == DISPLAY_COLOR_BW) {
				//if (dual_display_mode == DUAL_DISPLAY) {
				//	address_A = screen * (size << 1);
				//	size_image_total <<= 1;
				//} else {
	 				address_A = screen * size;
				//}
			} else {
				//if (dual_display_mode == DUAL_DISPLAY) {
				//	address_A = screen * (size << 2);
				//	size_image_total <<= 2;
				//} else {
					address_A = screen * (size << 1);
					size_image_total <<= 1;
				//}
			}

			address_A += DISPLAY_INT_DATA_ADDRESS_OFFSET;
			//address_B = address_A + size; //dual_display_mode

			size_image = size_image_total;
			while (size_image) {
				num_bytes = size_image;
				if (num_bytes > DISPLAY_BUFFER_SIZE) {
					num_bytes = DISPLAY_BUFFER_SIZE;
				}

				eeprom_read(address_A,image_data,num_bytes);

				for(i=0;i<num_bytes;i++) {
					crc32_compute(&crc, image_data[i]);
				}

				address_A += num_bytes;
				size_image -= num_bytes;
			}

/*
			if (dual_display_mode == DUAL_DISPLAY) {
				// address will still be valid
				size_image = size_image_total;
				while (size_image) {
					num_bytes = size_image;
					if (num_bytes > DISPLAY_BUFFER_SIZE) {
						num_bytes = DISPLAY_BUFFER_SIZE;
					}

					eeprom_read(address_B,image_data,num_bytes);

					for(i=0;i<num_bytes;i++) {
						crc32_compute(&crc, image_data[i]);
					}

					address_B += num_bytes;
					size_image -= num_bytes;
				}
			}
*/
		}
	}

	communications_set_register(REGISTER_CRC_RESULT, crc);
}




