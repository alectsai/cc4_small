//----------------------------------------------------------------------------
// display.h
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define DISPLAY_SIZE_1p54		0x00
#define DISPLAY_SIZE_2p13		0x01
#define DISPLAY_SIZE_2p90		0x02
#define DISPLAY_SIZE_4p20		0x03

#define DISPLAY_INT_DATA_ADDRESS_OFFSET		(0x00000800)
#define DISPLAY_EXT_DATA_ADDRESS_OFFSET		(0x00010000)
#define DISPLAY_EXT_RLE_DATA_ADDRESS_OFFSET	(0x00090000)

#define DISPLAY_COLOR_BW		0x00
#define DISPLAY_COLOR_BWR       0x01
#define DISPLAY_COLOR_BWY       0x02

#define DISPLAY_CONFIGURATION_ONE_DISPLAY				0x00
#define DISPLAY_CONFIGURATION_TWO_DISPLAYS_LEFT_RIGHT	0x01
#define DISPLAY_CONFIGURATION_TWO_DISPLAYS_TOP_BOTTOM	0x02

#define	WEIFENG_RESET_PULSE_WIDTH	100
#define	LONG_RESET_MILLISECONDS		1000

extern uint8_t display_size;
extern uint8_t display_color;
//extern uint8_t display_configuration; //dual_display_mode
extern uint8_t display_num_screens;
extern uint32_t display_time_since_last_render;
extern uint8_t display_render_complete;



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define DISPLAY_DC		(P1_bit.no3)
#define DISPLAY_RESET		(P2_bit.no3)
#define DISPLAY_BUSY_0		(P12_bit.no1)
#define DISPLAY_BUSY_1		(P13_bit.no7)
#define DISPLAY_SPI_CLK		(P0_bit.no0)
#define DISPLAY_SPI_MOSI	(P0_bit.no2)
#define DISPLAY_SPI_CS_0	(P2_bit.no0)
//#define DISPLAY_SPI_CS_1	(P2_bit.no2) //dual_display_mode

#define DISPLAY_SPI_DIR_MOSI (PM0_bit.no2)

#define DISPLAY_SPI_INPUT	1
#define DISPLAY_SPI_OUTPUT	0

#define DISPLAY_SPI_DEVICE_CS_0		0x01
//#define DISPLAY_SPI_DEVICE_CS_1		0x02 //dual_display_mode
//#define DISPLAY_SPI_DEVICE_CS_ALL	(DISPLAY_SPI_DEVICE_CS_0 | DISPLAY_SPI_DEVICE_CS_1) //dual_display_mode


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define DISPLAY_CLEAR_SCREEN_WHITE_VALUE 0x00
#define DISPLAY_CLEAR_SCREEN_BLACK_VALUE 0xFF


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void display_initialize(void);
void display_configure(uint16_t type);
void display_render(uint8_t screen);
void display_power_on_reset(void);
void display_power_off(void);
void display_shift_one_byte(uint8_t byte);
void display_clear_screen(uint8_t screen);
void display_copy_screen(uint32_t cmd);
void display_crc(uint32_t cmd);
//void display_spi_transfer(uint8_t device, uint8_t *buffer_out, uint16_t size, uint8_t continuation); //dual_display_mode
//void display_spi_read(uint8_t device, uint8_t *buffer_in, uint16_t size, uint8_t continuation); //dual_display_mode
void display_spi_transfer(uint8_t *buffer_out, uint16_t size, uint8_t continuation);
void display_spi_read(uint8_t *buffer_in, uint16_t size, uint8_t continuation);


