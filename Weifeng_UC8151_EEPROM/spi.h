//----------------------------------------------------------------------------
// spi.h
//----------------------------------------------------------------------------



//----------------------------------------------------------------------------
// Rev 1 PCB uses one SPI port to service both the memory and the display.
// Rev 2 PCB separates the two. The SPI port is used only for the memory and
// two I/O lines are used to bit-bang SPI for the display.
//
// I hate to use compile flags, but there is no way to auto-detect this
// hardware change. And since:
//
// a) We already have several hundred tags with the old PCBs.
// b) Without knowing what type of PCB is installed, we cannot access the
//    memory with risking degrading the display.
//
// Comment the following define out if using the Rev 2 PCB.
//----------------------------------------------------------------------------
//#define SPI_SHARED_SPI_PORT



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define SPI_DEVICE_MEMORY_CS   0x01

#define SPI_POWER_OFF 0
#define SPI_POWER_ON  1

#define SPI_TRANSFER_NO_CONTINUATION 0
#define SPI_TRANSFER_CONTINUATION    1



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define SPI_SCLK        (P1_bit.no0)
#define SPI_MISO        (P1_bit.no1)
#define SPI_MOSI        (P1_bit.no2)
#define SPI_MEMORY_CS   (P2_bit.no1)

#define SPI_DIR_MEMORY_CS (PM2_bit.no1)


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
extern void spi_initialize(void);
extern void spi_power(uint8_t state);
extern void spi_transfer(uint8_t device, uint8_t *buffer_out, uint8_t *buffer_in, uint16_t size, uint8_t continuation);
