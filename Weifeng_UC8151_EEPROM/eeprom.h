#ifndef _EEPROM_H_
#define _EEPROM_H_

//----------------------------------------------------------------------------
// eeprom.h
//----------------------------------------------------------------------------



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define EEPROM_POWER_OFF 0
#define EEPROM_POWER_ON  1

#define EEPROM_POWER_TIMEOUT_COUNT 10 // in increments of 1/4 seconds

extern uint8_t eeprom_power_timeout;



//----------------------------------------------------------------------------
// The first two kilobytes of EEPROM are reserved for "persistent" memory.
// This memory is configuration data that is maintained through a power
// cycle.
//
// Starting at address 0x00000000 (addressed externallay as 0xF800) is
// the command/waveform tables for the ultrachip controller.
// Persistent state variables are allocated from the top of the persistent
// memory area.
//----------------------------------------------------------------------------
#define EEPROM_PERSISTENT_MEMORY_OFFSET 0x00000800



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
extern uint32_t eeprom_memory_size;
extern uint16_t eeprom_page_size;
extern uint8_t eeprom_address_size;


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void eeprom_initialize(void);
void eeprom_configure_size(void);
void eeprom_power(uint8_t state);
void eeprom_write(uint32_t address, uint8_t *buffer, uint16_t size);
void eeprom_read(uint32_t address, uint8_t *buffer, uint16_t size);
void eeprom_read_uint32(uint32_t address, uint32_t *value);
void eeprom_write_uint32(uint32_t address, uint32_t value);

#endif
