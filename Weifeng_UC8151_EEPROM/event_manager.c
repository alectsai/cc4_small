//----------------------------------------------------------------------------
// event_manager.c
//----------------------------------------------------------------------------
#include "all_includes.h"
#include "provisioning.h"


#define PROVISIONING_CHECK_PERIOD 	3600		// 4 * 60 * 15 -> 15 minutes
#define SYNC_CHECK_PERIOD 2400			// 4 * 60 * 10 -> 10 minutes
#define SYNC_CHECK_TIMEOUT 288			// 288 * 10 minutes -> 48 hours
#define ABL_KICK_COUNT_THRESHOLD 	14400		// 1 hour
#define TAG_ID_CHECK_PERIOD 480		// 4 * 60 * 2 -> 2 minutes

//----------------------------------------------------------------------------
// Set option bytes
//----------------------------------------------------------------------------
#pragma location = "OPTBYTE"
//__root const uint8_t opbyte0 = 0x6FU;	 // disable WDT
__root const uint8_t opbyte0 = 0x7FU;	 // enable WDT, no interrupt, maximum timout (3.8 seconds)

#pragma location = "OPTBYTE"
__root const uint8_t opbyte1 = 0xFFU;	 // Low voltage detect disabled
//__root const uint8_t opbyte1 = 0x3FU;	 // Reset on low voltage detect

#pragma location = "OPTBYTE"
//__root const uint8_t opbyte2 = 0xE9U;	 // high speed mode, 16 MHz	2.4V+
__root const uint8_t opbyte2 = 0xAAU;	 // low speed mode, 8 MHz	1.8V+

#pragma location = "OPTBYTE"
__root const uint8_t opbyte3 = 0x04U;



//----------------------------------------------------------------------------
// Set security ID
//----------------------------------------------------------------------------
#pragma location = "SECUID"
__root const uint8_t secuid[10] = {0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U, 0x00U};



//----------------------------------------------------------------------------
// Dual display state variable.
//----------------------------------------------------------------------------
//uint8_t dual_display_mode;
uint8_t temperature_sampling_count;
uint16_t cc4_abl_kick_count;
uint16_t cc4_no_sync_check;
uint16_t cc4_no_sync_count;
uint32_t cc4_previous_sync_counter;
uint16_t provisioning_check;

uint16_t cc4_tag_id_check;

//----------------------------------------------------------------------------
// Event timing state variables.
//----------------------------------------------------------------------------
uint32_t event_immediate_end_time;
uint8_t event_active_screen;
uint8_t event_sequence_step;
uint8_t event_sequence_step_count;
uint32_t event_sequence_time;

#define		UNUSED_SIZE	284   //484, alec, cause cc4_tag_id_check overflow	// Keep this as big as possible w/o causing a linker error
uint8_t		Unused[UNUSED_SIZE];

//----------------------------------------------------------------------------
// Called by the IAR startup code before main() gets called.
//----------------------------------------------------------------------------
int16_t __low_level_init(void) {

	Unused[0] = 0;	// This forces the linker to include the "Unused" array in bss zero

	return 1U;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void main(void) {
	uint32_t value;
	uint32_t value2;
	uint32_t time;
	uint32_t next_time;
	uint8_t screen;
	uint8_t immediate_event_active_flag;
	uint8_t change;
	int32_t temperature;

	// Initialization
	__disable_interrupt();
	PIOR = 0x00;
	ADPC = 0x00;

	HOCODIV = 0x02;

	// Set fMX
	CMC = 0x00U;
	MSTOP = 1U;
	// Set fMAIN
	MCM0 = 0U;
	OSMC = 0x10U;
	// Set fIH
	HIOSTOP = 0U;

	//............................................................................
	// Port/pin mapping is as follows (all unlisted ports/bits are unavailable):
	//............................................................................
	// P0 bit 0,	pin 7	- OUTPUT			display SPI SCLK (bit-banged) (formerly test point 35)
	// P0 bit 1,	pin 8	- OUTPUT			open drain I2C master SCL (formerly CC4 slave SCL)
	// P0 bit 2,	pin 9	- OUTPUT			display SPI MOSI (bit-banged) (formerly test point 37)
	// P0 bit 3,	pin 10 - OUTPUT			display power (active low, gates power to display)
	P0 = 0x08;
	PM0 = 0xF0;
	POM0 = 0x02;
	PU0 = 0x02;

	// P1	bit 0, pin 17 - OUTPUT			SPI master SCLK
	// P1	bit 1, pin 16 - INPUT			 SPI master MISO
	// P1	bit 2, pin 15 - OUTPUT			SPI master MOSI
	// P1	bit 3, pin 14 - OUTPUT			display D/C*
	// P1	bit 4, pin 13 - OUTPUT			display SPI CS2 (for 4.2" display only)
	P1 = 0x15;
	PM1 = 0xE2;
	PU1 = 0x00;
	POM1 = 0x00;
	PIM1 = 0x00;
	PMC1 = 0x00;

	// P2	bit 0, pin 21 - OUTPUT			display SPI CS1
	// P2	bit 1, pin 20 - OUTPUT			MEMORY SPI CS
	// P2	bit 2, pin 19 - OUTPUT			level shifter enable (active low)
	// P2	bit 3, pin 18 - OUTPUT			display reset (active low)
	P2 = 0x06;
	ADPC = 0x01;
	PM2 = 0x00;

	// P4	bit 0, pin 24 - OUTPUT			TOOL
	// P4	bit 1, pin 23 - I/O				 open drain I2C master SDA (CC4 slave SDA)
	// P4	bit 2, pin 22 - OUTPUT			EEPROM power (active low, gates power to EEPROM) 
	P4 = 0x04;
	PM4 = 0xF8;
	PU4 = 0x02;
	POM4 = 0x02;
	PMC4 = 0x00;

	// P6	bit 0, pin 11 - INPUT			 I2C slave SCL (CC4 master SCL)
	// P6	bit 1, pin 12 - I/O				 I2C slave SDA (CC4 master SDA)
	P6 = 0x00;
	PM6 = 0x03;

	// P12 bit 1, pin 4	- INPUT			 display busy
	// P12 bit 2, pin 3	- INPUT			 not used
	// P12 bit 5, pin 1	- INPUT			 MCU reset
	PU12 = 0x11;

	// P13 bit 7, pin 2	- INPUT			 wake pushbutton

	//............................................................................
	// Currently, all four interrupt input pins are disabled. INTP0 is designated
	// for use with a wakeup pushbutton, but it has not been implemented in
	// hardware yet.
	//............................................................................
	//
	// disable INTP0 operation
	PMK0 = 1U;
	//
	// clear INTP0 interrupt flag
	PIF0 = 0U;
	//
	// disable INTP1 operation
	PMK1 = 1U;
	//
	// clear INTP1 interrupt flag
	PIF1 = 0U;
	//
	// disable INTP2 operation
	PMK2 = 1U;
	//
	// clear INTP2 interrupt flag
	PIF2 = 0U;
	//
	// disable INTP3 operation
	PMK3 = 1U;
	//
	// clear INTP3 interrupt flag
	PIF3 = 0U;

	// order of initialization is important
	timer_initialize();
	spi_initialize();
	i2c_master_initialize();

    timer_delay(300);   // Let the CC4 come up

	// leave this check for the CC here, but there is no task set for it..
	// probably better to check during provisioning
	if (i2c_master_read_register(0x0000,&value) == I2C_M_NACK) {
		error_report(ERROR_COM_NO_CC4);
	}

    // Fix for bad CC4 dies, where x1020 must be set to a non-default value for ABL to work.
    while( i2c_master_write_register(0x1020,0x2C000301) == I2C_M_NACK )
    {
		error_report(ERROR_COM_NO_CC4);
	}

	// EEPROM is powered from MCU GPIO line.
	POWER_EEPROM_ENA = 0;

	i2c_slave_initialize();
	eeprom_initialize();
        provisioning_determine_address_length();
	communications_initialize();
	display_initialize();

	event_active_screen = 0xFF;

	//temperature_sampling_count = 0;	// In bss zero
	//cc4_abl_kick_count = 0;
	//cc4_no_sync_check = 0;
	//cc4_no_sync_count = 0;
	//cc4_previous_sync_counter = 0;
	//provisioning_check = 0;
	//cc4_tag_id_check = 0;

	IAWCTL = 0x80U;
	__enable_interrupt();

	// try to reprovision from EEPROM
	// calls the display_configure() to set tag parameters relating to the display and memory
	// it would be nice to call this before the timer, but we need the timer for 
	// the battery evaluation render, so...
	provisioning_reprovision();

	// never-ending event loop
	while (1) {
		change = 0;

		if (i2c_slave_transfer_end_count > 0) {
			__disable_interrupt();
			--i2c_slave_transfer_end_count;
			__enable_interrupt();

			change = 1;
		}

		if (change || timer_increment) {
			communications_process();

			if (temperature_sampling_count > 240) {
				// Update temperature once per minute (approximately)
				i2c_master_read_register(0x01A0,&value);

				// 8-bit adc with offest already corrected in the CC4
				temperature = ((value >>	16) & 0xFF);

				// Convert to millidegress C
				temperature = 20000 + ((((temperature * 1000) - 114821) * 581) / 803);
				communications_set_register(REGISTER_TEMPERATURE,(uint32_t)temperature);

				temperature_sampling_count = 0;
			}

                        //check for render count clear trigger bit.
                        clearMiscTableContent();
                        
			// check for provisioning related tasks
			value = communications_get_register(REGISTER_PROVISIONING);
			if ((value & 0x00000007U)!=0) {
				if ((value & PROVISIONING_FLAGS_FORCE_ACTIVATION_SNAPSHOT)!=0) {
					provisioning_check_refresh();

				} else if ((value & PROVISIONING_FLAGS_FORCE_REPROVISION)!=0) {
					provisioning_reprovision();

				} else if ((value & PROVISIONING_FLAGS_FORCE_RESET)!=0) {
					// execute TRAP instruction (invalid instruction) which triggers a reset
					asm("db 0xFF");
				}

				// be sure to get a fresh copy of the register because provsioning affects it
				value = communications_get_register(REGISTER_PROVISIONING);
				communications_set_register(REGISTER_PROVISIONING, (value&0xFFFFFF00U));
			
				// whatever task performed, it invalidates the count, so reset it..
				provisioning_check = 0;

			} else {
				if (provisioning_check >= PROVISIONING_CHECK_PERIOD) {
	
					if ((value&PROVISIONING_FLAGS_UPDATE_ACTIVATION_DATA)!=0) {
						provisioning_check_refresh();
					}
	
					provisioning_check = 0;
				}
			}

			if (cc4_tag_id_check >= TAG_ID_CHECK_PERIOD)
			{
				// Check CC4  TAG ID once every 2 minutes
				cc4_tag_id_check = 0;

				i2c_master_read_register(0x0,&value);
				value &= 0x0000FF00;

				if (value == 0x0000FF00)
				{
					provisioning_reprovision();				
				}
			}

			if (cc4_no_sync_check >= SYNC_CHECK_PERIOD)
			{
				// Check CC4 sync counter once every ten minutes
				cc4_no_sync_check = 0;
#if 0
				i2c_master_read_register(0x0,&value);
				value &= 0x0000FF00;

				if (value == 0x0000FF00)
				{
					provisioning_reprovision();				
				}
#endif
				i2c_master_read_register(0x1218,&value);

				if (value == 0)
				{
					// Do not bother with the low power check if the radio is off
					cc4_no_sync_count = 0;
				}
				else
				{
					i2c_master_read_register(0x11AC,&value);

					if (value == cc4_previous_sync_counter)
					{
						if (cc4_no_sync_count < SYNC_CHECK_TIMEOUT)
						{
							++cc4_no_sync_count;
							if (cc4_no_sync_count == SYNC_CHECK_TIMEOUT)
							{
								// If lost for 48 hours, put cc4 radio into a lower power state
								// Adjust so radio is only turned on once every 28 seconds
								i2c_master_write_register(0x1214,0x06006ACF);
							}
						}
					}
					else // From "if (value == cc4_previous_sync_counter)"
					{
						// tag is in sync
						if (cc4_no_sync_count >= SYNC_CHECK_TIMEOUT)
						{
							// restore radio mode
							// Set channel hunt interval back to default.
							i2c_master_write_register(0x1214,0x0600040E);
						}

						cc4_no_sync_count = 0;

					} // End of "if (value == cc4_previous_sync_counter)"

					cc4_previous_sync_counter = value;

				} // End of "if (value == 0)" - "value" read from CC4 0x1218

			} // end of "if (cc4_no_sync_check >= SYNC_CHECK_PERIOD)"

			if (cc4_abl_kick_count > ABL_KICK_COUNT_THRESHOLD) {
				// Every hour check the CC4 to see if the radio is off. If it is off,
				// turn it on for two seconds. This is needed since the ABL will eventually
				// hang while the radio is off.
				i2c_master_read_register(0x1218,&value);

				if (value == 0) {
					// Two seconds at 1.024ms / tic
					i2c_master_write_register(0x1218,1953);
				}

				cc4_abl_kick_count = 0;
			}

			// Check for clear screen event
			value = communications_get_register(REGISTER_CLEAR_SCREENS);
			if (value & 0x01) {
				if ((value & 0x04) == 0) {
					for (screen = 0; screen < display_num_screens; ++screen) {
						if (value & (((uint32_t)0x100) << screen)) {
							display_clear_screen(screen);
						}
					}

				} else {
					provisioning_clear(value>>8);
				}

				value = 0;
				communications_set_register(REGISTER_CLEAR_SCREENS,value);
			}

			// check for a copy screen event
			value = communications_get_register(REGISTER_COPY_SCREEN);
			if (value & 0x01) {

				display_copy_screen(value);
				value &= 0xFFFFFFFE;
				communications_set_register(REGISTER_COPY_SCREEN,value);
			}

			// Check for CRC event
			value = communications_get_register(REGISTER_CRC_CMD);
			if (value & 0x01) {
				display_crc(value);
				value &= 0xFFFFFFFE ;
				communications_set_register(REGISTER_CRC_CMD,value);
			}

			// Do not render the screen more than once every 5 seconds
			if (display_render_complete == 1) {
				display_time_since_last_render = 0;
				display_render_complete = 0;
			}

			// 5 seconds in quarter second intervals
			if (display_time_since_last_render > 20) {


				// Check for immediate display event (aka "tickle")
				immediate_event_active_flag = 0;
				value = communications_get_register(REGISTER_IMMEDIATE_DISPLAY_EVENT);

				if (value & 0x01) {
					// trigger has been set
					screen = (uint8_t)((value >> 1) & 0x0F);
					display_render(screen);
					value &= ~0x41;
					value |= 0x20;
					communications_set_register(REGISTER_IMMEDIATE_DISPLAY_EVENT,value);
					event_immediate_end_time = communications_get_register(REGISTER_NETWORK_TIME);
					event_immediate_end_time += ((value & 0xFFFF0000) >> 6);
					immediate_event_active_flag = 1;

				} else if (value & 0x40) {
					// event has been aborted
					if (value & 0x20) {
						// aborting the event is only meaningful if it is active
						display_render(event_active_screen);
					}

					value &= ~0x60;
					communications_set_register(REGISTER_IMMEDIATE_DISPLAY_EVENT,value);
				}

				if (value & 0x20) {
					// immediate display event is active
					time = event_immediate_end_time - communications_get_register(REGISTER_NETWORK_TIME);
					if (time & 0x80000000) {
						// immediate event duration has elapsed, revert back to the active screen
						display_render(event_active_screen);
						value &= ~0x20;
						communications_set_register(REGISTER_IMMEDIATE_DISPLAY_EVENT,value);

					} else {
						// still active
						immediate_event_active_flag = 1;
					}
				}

				// Check for immediate render (in DD4 terminology, this is a "low priority immediate display event")
				value = communications_get_register(REGISTER_IMMEDIATE_RENDER);
				if (value & 0x01) {

					// trigger has been set. Check to see if one of the following is true:
					// a) uncondtional render bit is set
					// b) the screen being rendered is not the currently active screen
					// c) at least 30 seconds has passed since the last render
					if ((value & 0x02) || display_time_since_last_render > (30 * 4) || event_active_screen != (uint8_t)(value >> 8)) {

						// Kill off any scheduled or sequence events
						value2 = communications_get_register(REGISTER_SCHEDULED_DISPLAY_EVENT);
						if (value2 & 0x21) {
							// clear scheduled event active and trigger bits
							value2 &= ~0x21;
							communications_set_register(REGISTER_SCHEDULED_DISPLAY_EVENT,value2);
						}

						value2 = communications_get_register(REGISTER_SEQUENCE_EVENT_START_TIME);
						if (value2 & 0x07) {
							// clear sequence active, enable, and trigger bits
							value2 &= ~0x07;
							communications_set_register(REGISTER_SEQUENCE_EVENT_START_TIME,value2);
						}

						event_active_screen = (uint8_t)(value >> 8);
						if (immediate_event_active_flag != 1) {
							display_render(event_active_screen);
						}
					}
					value &= ~0x03;
					communications_set_register(REGISTER_IMMEDIATE_RENDER,value);
				}

				// Check for scheduled display event
				value = communications_get_register(REGISTER_SCHEDULED_DISPLAY_EVENT);
				if (value & 0x01) {
					// trigger bit set
					time = communications_get_register(REGISTER_NETWORK_TIME);
					time -= (value & 0xFFFF0000);
					if (!(time & 0x80000000)) {
						// event has been triggered
						event_active_screen = ((value >> 1) & 0x0F);
						if (immediate_event_active_flag != 1) {
							display_render(event_active_screen);
						}

						value &= ~0x01;
						value |= 0x20;
						communications_set_register(REGISTER_SCHEDULED_DISPLAY_EVENT,value);

						value = communications_get_register(REGISTER_SEQUENCE_EVENT_START_TIME);
						if (value & 0x06) {
							// clear sequence active and enable bits
							value &= ~0x06;
							communications_set_register(REGISTER_SEQUENCE_EVENT_START_TIME,value);
						}
					}
				}

				// Check for sequence event
				value = communications_get_register(REGISTER_SEQUENCE_EVENT_START_TIME);
				if (value & 0x01) {

					// trigger bit set
					time = communications_get_register(REGISTER_NETWORK_TIME);
					event_sequence_time = (value & 0xFFFFFFE0);
					time -= event_sequence_time;
					if (!(time & 0x80000000)) {
						// event has been triggered
						value |= 0x04;
						value &= ~0x01;
						communications_set_register(REGISTER_SEQUENCE_EVENT_START_TIME,value);

						value = communications_get_register(REGISTER_SEQUENCE_CONFIG);
						event_sequence_step = ((value >> 12) & 0x0F);
						event_sequence_step_count = (value & 0x0F);

						// Here we perform a "catch up" for the case where the sequence start time was
						// is the "distant" past.
						while (1) {
							next_time = communications_get_register(REGISTER_SEQUENCE_SCREEN_01_TIME + (event_sequence_step >> 1));
							if (event_sequence_step & 0x01) {
								next_time <<= 16;
							}

							next_time &= 0xFFFF0000;

							if (time > next_time) {
								// skip a step
								time -= next_time;
								event_sequence_time += next_time;
								++event_sequence_step;
								--event_sequence_step_count;
								if (event_sequence_step_count == 0) {
									event_sequence_step = ((value >> 12) & 0x0F);
									event_sequence_step_count = (value & 0x0F);
								}

							} else {
								// Done with catch up
								break;
							}
						}

						if (event_sequence_step < 8) {
							event_active_screen = ((communications_get_register(REGISTER_SEQUENCE_SCREEN_ORDER) >> ((7 - event_sequence_step) << 2)) & 0x0F);

						} else if (event_sequence_step == 8) {
							event_active_screen = ((value >> 28) & 0x0F);

						} else if (event_sequence_step == 9) {
							event_active_screen = ((value >> 24) & 0x0F);

						} else {
							// nothing to display
							event_active_screen = 0x0F;
						}

						value &= 0xFFFFF0FF;
						value |= ((event_active_screen) << 8);
						communications_set_register(REGISTER_SEQUENCE_CONFIG,value);

						if (immediate_event_active_flag != 1) {
							display_render(event_active_screen);
						}

						value = communications_get_register(REGISTER_SCHEDULED_DISPLAY_EVENT);
						if (value & 0x20) {
							// clear scheduled event active bit
							value &= ~0x20;
							communications_set_register(REGISTER_SCHEDULED_DISPLAY_EVENT,value);
						}
					}

				} else if ((value & 0x06) == 0x06) {
					// Event is active and sequencing is enabled

					// Now check to see if the squence can advance.
					if (event_sequence_step < 10) {
						time = communications_get_register(REGISTER_SEQUENCE_SCREEN_01_TIME + (event_sequence_step >> 1));
						if (event_sequence_step & 0x01) {
							time <<= 16;
						}

						time &= 0xFFFF0000;
						time += event_sequence_time;

						if (!((communications_get_register(REGISTER_NETWORK_TIME) - time) & 0x80000000)) {
							// time for the next step in the sequence
							value = communications_get_register(REGISTER_SEQUENCE_CONFIG);
							while (1) {
								event_sequence_time = time;
								++event_sequence_step;
								--event_sequence_step_count;
								if (event_sequence_step_count == 0) {
									event_sequence_step = ((value >> 12) & 0x0F);
									event_sequence_step_count = (value & 0x0F);
								}

								//	Here we also cover the case where the network time had a large change while a sequene is in progress.
								next_time = communications_get_register(REGISTER_SEQUENCE_SCREEN_01_TIME + (event_sequence_step >> 1));
								if (event_sequence_step & 0x01) {
									next_time <<= 16;
								}

								next_time &= 0xFFFF0000;
								next_time += event_sequence_time;
								if ((communications_get_register(REGISTER_NETWORK_TIME) - next_time) & 0x80000000) {
									// end of catch up
									break;
								}
								time = next_time;
							}

							if (event_sequence_step < 8) {
								event_active_screen = ((communications_get_register(REGISTER_SEQUENCE_SCREEN_ORDER) >> ((7 - event_sequence_step) << 2)) & 0x0F);

							} else if (event_sequence_step == 8) {
								event_active_screen = ((value >> 28) & 0x0F);

							} else if (event_sequence_step == 9) {
								event_active_screen = ((value >> 24) & 0x0F);

							} else {
								// nothing to display
								event_active_screen = 0x0F;
							}

							value &= 0xFFFFF0FF;
							value |= ((event_active_screen) << 8);
							communications_set_register(REGISTER_SEQUENCE_CONFIG,value);

							if (immediate_event_active_flag != 1) {
								display_render(event_active_screen);
							}
						}

					} else {
						// nothing to display, sequence will not progress
						event_active_screen = 0x0F;
					}
				}
			}
		}

		// Service the WDT
		WDTE = 0xAC;

		// This won't necessarily power off the memory since there is also
		// a timeout counter that keeps the memory on for a while after
		// each access.
		eeprom_power(EEPROM_POWER_OFF);

		// This call will put the MCU to sleep until an interrupt wakes it up
		if (i2c_slave_state == I2C_SLAVE_STATE_IDLE && (IICS0 & 0xB2) == 0x00) {
			WUP0 = 1;
			__no_operation();
			__no_operation();
			__no_operation();
			__stop();

		} else {
			__halt();
		}

	}
}
