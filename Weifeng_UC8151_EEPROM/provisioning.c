//-----------------------------------------------------------------------------------
// provisioning.c - functions that are used for factory and activation reprovisioning 
//-----------------------------------------------------------------------------------
#include "all_includes.h"
#include "provisioning.h"
#include "eeprom.h"


extern void eeprom_read_uint32(uint32_t addr, uint32_t *value);

#define I2C_ID_CC4			0x41U
#define I2C_ID_UC			0x20U
#define I2C_ID_CMD_DELAY	0x00U



uint8_t provisioning_check_factory(void) {
	uint32_t record_count;
	uint32_t record_chksum;
	uint32_t chksum = 0UL;
	uint32_t value;
	uint32_t index;
	uint16_t i;

	uint8_t retval = STATUS_OK;

	// Factory Provisioning First
	// first read the number of record
	index = PROVISIONING_FACTORY_OFFSET;
	eeprom_read_uint32(PROVISIONING_FACTORY_OFFSET,&value);
	chksum = value;
	index += 4U;
	
	// The Factory ID at offset 4, running 6 bytes is not included in the checksum
	//eeprom_read_uint32(PROVISIONING_FACTORY_OFFSET+4U,&value);
	//chksum += value;

	// read the record count
	eeprom_read_uint32(index,&value);
	chksum += value;
	index += 4U;

	// mask off the unused bits
	record_count = value & 0x0FFUL;

	if (record_count > PROVISIONING_FACTORY_MAX_RECORD_COUNT) {
		retval = STATUS_FAIL;

	} else {
		// every register has two uint_32's (register address and data)
		// so double the count and add the 12 preceeding bytes
		record_count <<= 1;
		for(i=0;i<record_count;i++) {
			eeprom_read_uint32(index,&value);
			chksum += value;
			index += 4U;
		}

		// invert the checksum to avoid validating an erased block
		chksum = ~chksum;

		// read the checksum value at the end of the list
		eeprom_read_uint32(index,&record_chksum);
		index += 4U;

		if (chksum != record_chksum) {
			retval = STATUS_FAIL;
		}
	}

	return retval;
}


uint8_t provisioning_check_factory_unique(void) {
	uint32_t record_count;
	uint32_t record_chksum;
	uint32_t chksum = 0UL;
	uint32_t value;
	uint32_t index;
	uint16_t i;

	uint8_t retval = STATUS_OK;

	// Factory Provisioning First
	// now check the per-tag provisioning table
	if (retval == STATUS_OK) {
		index = PROVISIONING_FACTORY_UNIQUE_OFFSET;

		// read the first 4 bytes of the factory ID
		eeprom_read_uint32(index,&value);
		chksum = value;
		index += 4U;

		// read the last 2 bytes of the factory ID, and the record count
		eeprom_read_uint32(index,&value);
		chksum += value;
		index += 4U;
		record_count = (value & 0x0FF)<<1;

		// read the address/data pairs
		for(i=0;i<record_count;i++) {
			eeprom_read_uint32(index,&value);
			chksum += value;
			index += 4U;
		}

		// invert the checksum to avoid validating an erased block
		chksum = ~chksum;

		// read the checksum value at the end of the list
		eeprom_read_uint32(index,&record_chksum);
		index += 4U;

		if (chksum != record_chksum) {
			retval = STATUS_FAIL;
		}
	}

	return retval;
}



uint8_t provisioning_check_activation_table_address(void) {
	uint32_t record_count;
	uint32_t record_chksum;
	uint32_t chksum = 0UL;
	uint32_t value;
	uint32_t index;
	uint16_t i;

	uint8_t retval = STATUS_OK;

	// first read the number of records, this is common to both activation addresses and data
	eeprom_read_uint32(PROVISIONING_ACTIVATION_ADDRESS_OFFSET,&record_count);

	// start the checksum with the count number
	chksum = record_count;

	// mask off the flags and other unused bits
	record_count &= 0x0FF;

	if (record_count > PROVISIONING_ACTIVATION_MAX_RECORD_COUNT) {
		retval = STATUS_FAIL;

	} else {
		index = PROVISIONING_ACTIVATION_ADDRESS_RECORD_OFFSET;
		for(i=0;i<record_count;i++) {
			eeprom_read_uint32(index,&value);
			chksum += value;
			index += 4U;
		}

		// invert the checksum to avoid validating an erased block
		chksum = ~chksum;

		// read the checksum value at the end of the list
		eeprom_read_uint32(index,&record_chksum);

		if (chksum != record_chksum) {
			retval = STATUS_FAIL;
		}
	}

	return retval;
}




uint8_t provisioning_check_activation_table_data(void) {
	uint32_t record_count;
	uint32_t record_chksum;
	uint32_t chksum = 0UL;
	uint32_t value;
	uint32_t index;
	uint16_t i;
	uint8_t buf[1];

	uint8_t retval = STATUS_OK;


	// first read the number of records, this is common to both activation addresses and data
	eeprom_read(PROVISIONING_ACTIVATION_COUNT_OFFSET,buf,1);
	record_count = buf[0];

	// include the flags value in the checksum
	record_count++;

	if (record_count > PROVISIONING_ACTIVATION_MAX_RECORD_COUNT) {
		retval = STATUS_FAIL;

	} else {
		// the flags and data are contiguous
		index = PROVISIONING_ACTIVATION_DATA_OFFSET;
		for(i=0;i<record_count;i++) {
			eeprom_read_uint32(index,&value);
			chksum += value;
			index += 4U;
		}

		// invert the checksum to avoid validating an erased block
		chksum = ~chksum;

		// read the checksum value at the end of the list
		eeprom_read_uint32(index,&record_chksum);

		if (chksum != record_chksum) {
			retval = STATUS_FAIL;
		}
	}

	return retval;
}

//clear render count and render count failure in EEPROM 0xD04[31:0] and 0xD02[7:0]
//clear 0x40 and 0x08[20:13]

void clearMiscTableContent (void){
      uint32_t failRenderCount, clearRenderTigger;
      clearRenderTigger = communications_get_register(REGISTER_PROVISIONING);
      if(clearRenderTigger & PROVISIONING_CLEAR_RENDER_COUNT){
          // Clearing 0x08[20:13] render failure count and render status
          failRenderCount = communications_get_register(REGISTER_STATUS) &  STATUS_RENDER_FAIL_COUNT_MASK;
          communications_set_register( REGISTER_STATUS, failRenderCount );
          
          //clearing 0x40 render count register
          communications_set_register( REGISTER_RENDER_COUNT, 0x00000000U );
          
          //clear eeprom 0xD02 6 bytes
          eeprom_write_uint32(PROVISIONING_RENDER_FAIL_COUNT_OFFSET,0x00000000U);
          eeprom_write_uint32(PROVISIONING_RENDER_COUNT_OFFSET,0x00000000U);
          
          //clearing trigger bit          
          clearRenderTigger &= (uint32_t)(~PROVISIONING_CLEAR_RENDER_COUNT);
          communications_set_register( REGISTER_PROVISIONING, clearRenderTigger );
      }
}


// 
// Check the provisioning data and use it to reprovision the tag if it is valid
// 	Factory provisioning must match the checksum
//  Activation provisioning must be enabled and must match the checksum for both
//		registers and data
// 
uint8_t provisioning_reprovision(void) {
	uint32_t record_count;
	uint32_t record_address;
	uint8_t record_i2c_id;
	uint32_t record_value;
	uint32_t index;
	uint32_t index2;
	uint16_t i;
	uint32_t prov_flags;
	uint8_t status = STATUS_OK;
	uint8_t buf[2];

	provisioning_determine_address_length();

	prov_flags = communications_get_register(REGISTER_PROVISIONING);
	// clear the status flags, leave the config and action fields alone
	prov_flags &= 0x00FFFFFF;

	status = provisioning_check_factory();

	if (status == STATUS_OK) {
		prov_flags |= PROVISIONING_FLAGS_FACTORY_TABLE_VALID;

		//
		// Factory Provisioning START
		//
		// read and set the tag type 
		index = PROVISIONING_FACTORY_OFFSET;
		// skip the first 2
		index += 2U;

		eeprom_read(index,buf,2);
		index += 2U;

		record_value = ((uint32_t)buf[0]) << 24;
		record_value |= ((uint32_t)buf[1]) << 8;
		i2c_master_write_register(0x000000,record_value);
		

		display_configure((((uint16_t)buf[0])<<8) | buf[1]);

		// first read the number of records
		eeprom_read_uint32(index,&record_count);
		index += 4U;

		record_count &= 0x0FFUL;

		for(i=0;i<record_count;i++) {
			eeprom_read_uint32(index,&record_address);
			index+=4U;
			eeprom_read_uint32(index,&record_value);
			index+=4U;

			record_i2c_id = record_address>>24;
			record_address &= 0x00FFFFFFUL;

			if (record_i2c_id == I2C_ID_CC4) {
				i2c_master_write_register(record_address,record_value);
			} else if (record_i2c_id == I2C_ID_UC) {
				// the register address is 4x the register number 
				communications_set_register(record_address>>2,record_value);

				// grab the configuration bits if it's the provisioning config register
				if (record_address == (REGISTER_PROVISIONING<<2)) {
					// mask leftover status or command bits
					prov_flags |= (record_value & 0x00FFFF00);
				}

			} else if (record_i2c_id == I2C_ID_CMD_DELAY) {
				// need to delay by record_value ms
				timer_delay(record_value);
			}
		}
		//
		// Factory Provsioning DONE
		//


		//
		// Tag Specific Factory Provsioning START
		//
		status = provisioning_check_factory_unique();
		if (status == STATUS_OK) {
			//
			// Now handle the tag specific factory provisioning
			//
			prov_flags |= PROVISIONING_FLAGS_FACTORY_UNIQUE_TABLE_VALID;

			// Read and set the factory id
			index = PROVISIONING_FACTORY_UNIQUE_OFFSET;
			eeprom_read(index,buf,2);
			index += 2U;
			record_value = ((uint32_t)buf[0]) << 8;
			record_value |= buf[1];
			i2c_master_write_register(0x001190,record_value);
	
			eeprom_read_uint32(index,&record_value);
			i2c_master_write_register(0x001194,record_value);
			index += 4U;
	
			// skip 1 unused byte
			index += 1U;
	
			// read the number of tag specific records
			eeprom_read(index,buf,1);
			index += 1U;
			record_count = buf[0];
	
			for(i=0;i<record_count;i++) {
				eeprom_read_uint32(index,&record_address);
				index+=4U;
				eeprom_read_uint32(index,&record_value);
				index+=4U;
	
				record_i2c_id = record_address>>24;
				record_address &= 0x00FFFFFFUL;
	
				if (record_i2c_id == I2C_ID_CC4) {
					i2c_master_write_register(record_address,record_value);
				} else if (record_i2c_id == I2C_ID_UC) {
					// the register address is 4x the register number 
					communications_set_register(record_address>>2,record_value);
	
					// grab the configuration bits if it's the provisioning config register
					if (record_address == (REGISTER_PROVISIONING<<2)) {
						// mask leftover status or command bits
						prov_flags |= (record_value & 0x00FFFF00UL);
					}
	
				} else if (record_i2c_id == I2C_ID_CMD_DELAY) {
					// need to delay by record_value ms
					timer_delay(record_value);
				}
			}
		}
		//
		// Tag Specific/Unique Factory Provsioning DONE
		//


		//
		// Activation Provisioning START
		//

		// first check to see if the activation data is valid
		eeprom_read(PROVISIONING_ACTIVATION_FLAGS_OFFSET,buf,1);

		if ((buf[0] & PROVISIONING_ACTIVATION_FLAGS_VALID)!=0) {
			// read the number of activation registers
			eeprom_read(PROVISIONING_ACTIVATION_COUNT_OFFSET,buf,1);
			record_count = buf[0];

			// verify that the activation registers and data are good
			status = provisioning_check_activation_table_address();

			if (status == STATUS_OK) {
				prov_flags |= PROVISIONING_FLAGS_ACTIVATION_ADDRESS_TABLE_VALID;
				status = provisioning_check_activation_table_data();
			}

			if (status == STATUS_OK) {
				prov_flags |= PROVISIONING_FLAGS_ACTIVATION_DATA_TABLE_VALID;

				index = PROVISIONING_ACTIVATION_ADDRESS_RECORD_OFFSET;
				index2 = PROVISIONING_ACTIVATION_DATA_RECORD_OFFSET;

				for(i=0;i<record_count;i++) {
					eeprom_read_uint32(index,&record_address);
					index+=4U;

					eeprom_read_uint32(index2,&record_value);
					index2+=4U;
		
					record_i2c_id = record_address>>24;
					record_address &= 0x00FFFFFFUL;
		
					if (record_i2c_id == I2C_ID_CC4) {
						i2c_master_write_register(record_address,record_value);

					} else if (record_i2c_id == I2C_ID_UC) {
						// the register address is 4x the register number 
						communications_set_register(record_address>>2,record_value);

					} else if (record_i2c_id == I2C_ID_CMD_DELAY) {
						// need to delay by record_value ms
						timer_delay(record_value);
					}

				}
			}
		}

	}



	

	// if the tag is configured to render after repvisioning, let's do it..
	if ((prov_flags&PROVISIONING_FLAGS_ENABLE_POST_RENDER)!=0) {

		// read the last render data before it gets overwritten
		eeprom_read(PROVISIONING_LAST_RENDER_SCREEN_OFFSET, buf, 2);

		// reuse record_address to hold the stored last render voltage
		record_address = buf[1];

		// render the last screen
		display_render(buf[0]);

		// read the last render voltage register
		// reuse record_value to hold the current render voltage
		record_value = communications_get_register(REGISTER_RENDER_VOLTAGE);
		record_value &= 0x0FF;


		
		if (record_value > record_address) {
			if ((record_value-record_address) > PROVISIONING_BATTERY_CHANGE_DETECTION_THRESHOLD) {
				prov_flags |= PROVISIONING_FLAGS_BATTERY_CHANGED;
			}
		}
	}

	// indicate that the tag has been reprovisioned
	prov_flags |= PROVISIONING_FLAGS_REPROVISIONED;
	prov_flags &= 0xFF000000UL;

	// clear the status flags, leave the config and action fields alone
	record_value = communications_get_register(REGISTER_PROVISIONING) & 0x00FFFF00UL;

	// add the current config values
	prov_flags |= record_value;

	communications_set_register(REGISTER_PROVISIONING, prov_flags);


	return status;
}





void provisioning_check_refresh(void) {
	uint32_t value;
	uint32_t record_address;
	uint32_t record_value;
	uint32_t record_chksum;
	uint8_t record_i2c_id;
	uint32_t chksum = 0UL;
	uint8_t count;
	uint8_t i;
	uint32_t index;
	uint32_t index2;
	uint8_t buf[1];

	// validate the addresses
	uint8_t ok = provisioning_check_activation_table_address();

	if (ok == STATUS_OK) {
		eeprom_read(PROVISIONING_ACTIVATION_COUNT_OFFSET,buf,1);
		count = buf[0];


		// is provisioning update enabled?
		// do we want to key off of the register?
		//		if (((value&PROVISIONING_FLAGS_ACTIVE)!=0) && (count!=0)) {	
		// 			//check the data table
		ok = provisioning_check_activation_table_data();

		index = PROVISIONING_ACTIVATION_ADDRESS_RECORD_OFFSET;
		index2 = PROVISIONING_ACTIVATION_DATA_RECORD_OFFSET;

		for(i=0;i<count;i++) {
			eeprom_read_uint32(index,&record_address);

			// if the stored data is valid, read it for comparison
			if (ok == STATUS_OK) {
				eeprom_read_uint32(index2,&record_value);
			}

			record_i2c_id = record_address>>24;
			record_address &= 0x00FFFFFFUL;

			if (record_i2c_id == I2C_ID_CC4) {
				// what should we do if the read fails??? FIXME, try again in 5 minutes?
				i2c_master_read_register(record_address,&value);

			} else if (record_i2c_id == I2C_ID_UC) {
				// the register address is 4x the register number 
				value = communications_get_register(record_address>>2);
			}

			// if the stored data is valid, do the comparison and save the value;
			if (!((ok == STATUS_OK) && (value == record_value))) {
				eeprom_write_uint32(index2,value);
				record_value = value;
			}

			chksum += record_value;

			index+=4U;
			index2+=4U;
		}

		// make sure to include the valid flag stored in the first word
		chksum += PROVISIONING_ACTIVATION_FLAGS_VALID;

		chksum = ~chksum;

		// only read the old checksum if it was valid
		if (ok == STATUS_OK) {
			eeprom_read_uint32(index2,&record_chksum);
		}

		if (!((ok == STATUS_OK) && (chksum == record_chksum))) {
			// write the valid flag too
			buf[0] = 1;
			eeprom_write(PROVISIONING_ACTIVATION_FLAGS_OFFSET,buf,1);


			eeprom_write_uint32(index2,chksum);
		}

	}
}


uint8_t provisioning_determine_address_length(void) {
	uint8_t length = 0;
	uint8_t buf[2];

	uint8_t old_size = eeprom_address_size;
	buf[0] = 0x00;
	buf[1] = 0x00;

	eeprom_address_size = 2;
	eeprom_read(PROVISIONING_FACTORY_OFFSET,buf,2);

	if ((buf[0] == PROVISIONING_MAGIC_0) && (buf[1] == PROVISIONING_MAGIC_1)) {
		length = 2;

	} else {
		eeprom_address_size = 3;
		eeprom_read(PROVISIONING_FACTORY_OFFSET,buf,2);

		if ((buf[0] == PROVISIONING_MAGIC_0) && (buf[1] == PROVISIONING_MAGIC_1)) {
			length = 3;

		} else {
			eeprom_address_size = old_size;
		}
	}

	return length;
}


void provisioning_clear(uint8_t pages) {
	uint16_t mask = 0x0001U;
	uint32_t address=0;

	while(mask!=0) {
		if ((pages&mask)!=0) {
			eeprom_write(address,DISPLAY_CLEAR_SCREEN_WHITE_VALUE,256); 
			address += 256;
		}

		mask <<= 1;
	}
}


