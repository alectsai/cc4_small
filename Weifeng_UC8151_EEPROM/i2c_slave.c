//----------------------------------------------------------------------------
// i2c_slave.c
//----------------------------------------------------------------------------
#include "all_includes.h"



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
uint32_t volatile i2c_slave_address;
uint8_t volatile i2c_slave_state = I2C_SLAVE_STATE_IDLE;
uint8_t volatile i2c_slave_transfer_end_count = 0;



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void i2c_slave_initialize(void) {

	// supply IICA0 clock
	IICA0EN = 1U;

	// disable IICA0 operation for configuration
	IICE0 = 0U;

	// disable INTIICA0 interrupt
	IICAMK0 = 1U;

	// clear INTIICA0 interrupt flag
	IICAIF0 = 0U;

	// Set INTIICA0 high priority
	IICAPR10 = 0U;
	IICAPR00 = 0U;

	// Setup SCLA0, SDAA0 pins
	//
	// Set pin to 0 (bits 0 and 1)
	P6 &= 0xFCU;

	// Configure pins as inputs
	PM6 |= 0x03U;

	// Operate I2C in fast mode (slow mode only goes to 100 KHz, fast up to 400 KHz)
	SMC0 = 1U;

	// configure low/high cycle widths (probably not important for slave mode)
	IICWL0 = 0x26U;
	IICWH0 = 0x2BU;

	// run the IICA state machine at the full system clock (not half)
	IICCTL01 &= (uint8_t)~(0x01U);

	// assign slave address (shifted left one bit)
		SVA0 = (0x40U);

	// after enable, allow start generation without detecting stop condition
	// my guess this is priming the pump and not required for slave mode operation
	STCEN0 = 1U;

	// disable communication reservation (whatever that is...)
	IICRSV0 = 1U;

	// disable interrupt when stop condition detected
	SPIE0 = 0U;

	// interrupt timing:
	//	0: after data (before ACK)
	//	1: after ACK (1)
	WTIM0 = 1U;

	// ACK generation:
	//	0 disabled (manual)
	//	1 enabled	(automatic)
	ACKE0 = 1U;

	// Interrupt mask flag for IICA
	//	0: enabled
	//	1: disabled
	IICAMK0 = 0U;

	// enable IICA0 operation for configuration
	IICE0 = 1U;

	// Exit from operations
	// not sure what this is mean to do except reset the interface state
	LREL0 = 1U;

	// Configure SCLA0, SDAA0 pins as outputs
	PM6 &= 0xFCU;
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//
// IAR RL78 Workbench 2.10.1 has a bug in the ISR code that fails to
//	 save scratchpad registers.	2.10.3 has a fix, but that's only for
//	 paying customers.
//	 The workaround is to force the old call format on it with __v1_call
//		 http://documentation.renesas.com/doc/DocumentServer/R20UT3407ED0104_RL78.pdf 
//
#pragma vector = INTIICA0_vect
__v1_call __interrupt static void i2c_slave_isr(void) {
	uint8_t value;
	uint8_t status;

	// The MCU uses clock stretching (they call it "wait") at the end of each byte.
	// The CC4 has a bug in it that cannot handle certain situtations if the clock
	// is stretched out too far. So here we attempt to reduce the amount of clock
	// stretch by reading input bytes as quickly as possible.
	if (i2c_slave_state != I2C_SLAVE_STATE_IDLE &&
			i2c_slave_state != I2C_SLAVE_STATE_READ_DATA) {

		value = IICA0;

		// cancel wait
		WREL0 = 1U;
	}

	// disable the wake up
	WUP0 = 0; 

	// wait 5 clock cycles
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();
	__no_operation();

	status = IICS0;

	// Check for stop condition
	if ((status & 0x01) != 0) {
		// disable interrupt when stop condition detected
		SPIE0 = 0U;

		if (i2c_slave_state == I2C_SLAVE_STATE_WRITE_DATA) {
			communications_transfer_end();
			++i2c_slave_transfer_end_count;

		} else if (i2c_slave_state == I2C_SLAVE_STATE_READ_DATA) {
			WREL0 = 1U;
		}

		i2c_slave_state = I2C_SLAVE_STATE_IDLE;

	} else {

		// Do we already have a matching address?
		if (i2c_slave_state == I2C_SLAVE_STATE_IDLE) {

			// apparently not
			if ((status & 0x20) != 0) {
				// just stop communications
				LREL0 = 1;

			} else if ((status & 0x10) != 0) {
				// We have an address match
				// enable interrupt when stop condition detected
				SPIE0 = 1U;

				// check for Tx/Rx mode
				//	 0: Write by master (rx by slave)
				//	 1: Read by master	(tx by slave)
				if ((status & 0x08) != 0) {
					i2c_slave_state = I2C_SLAVE_STATE_READ_DATA;

					// send the first byte
					IICA0 = communications_get_memory(i2c_slave_address);
					i2c_slave_address++;

				} else {
					// cancel wait
					WREL0 = 1U;

					// write by master
					i2c_slave_state = I2C_SLAVE_STATE_ID_WRITE_BY_MASTER;

					// invalidate I2C memory address
					i2c_slave_address = 0;
				}
			}

		} else if (i2c_slave_state == I2C_SLAVE_STATE_ID_WRITE_BY_MASTER) {
			i2c_slave_state = I2C_SLAVE_STATE_READ_ADDR_0;
			i2c_slave_address = value;

		} else if (i2c_slave_state == I2C_SLAVE_STATE_READ_ADDR_0) {
			i2c_slave_state = I2C_SLAVE_STATE_READ_ADDR_1;
			i2c_slave_address <<= 8;
			i2c_slave_address += value;

		} else if (i2c_slave_state == I2C_SLAVE_STATE_READ_ADDR_1) {
			i2c_slave_state = I2C_SLAVE_STATE_READ_ADDR_2;
			i2c_slave_address <<= 8;
			i2c_slave_address += value;

		} else if ((i2c_slave_state == I2C_SLAVE_STATE_READ_ADDR_2) || (i2c_slave_state == I2C_SLAVE_STATE_WRITE_DATA)) {
			if (i2c_slave_state == I2C_SLAVE_STATE_READ_ADDR_2) {
				communications_transfer_start(i2c_slave_address);
			}

			communications_transfer_update(value);
			i2c_slave_address++;
			i2c_slave_state = I2C_SLAVE_STATE_WRITE_DATA;

		} else if (i2c_slave_state == I2C_SLAVE_STATE_READ_DATA) {
			// check to see if we got an ACK
			if ((status & 0x04) != 0) {
				// this automatically cancels the wait
				IICA0 = communications_get_memory(i2c_slave_address);
				i2c_slave_address++;

			} else {
				// got a NACK
				WREL0 = 1U;
			}
		}
	}
}
