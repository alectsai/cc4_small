//----------------------------------------------------------------------------
// i2c_slave.h
//----------------------------------------------------------------------------



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
#define I2C_SLAVE_STATE_IDLE               0
#define I2C_SLAVE_STATE_ID_WRITE_BY_MASTER 1
#define I2C_SLAVE_STATE_READ_ADDR_0        2
#define I2C_SLAVE_STATE_READ_ADDR_1        3
#define I2C_SLAVE_STATE_READ_ADDR_2        4
#define I2C_SLAVE_STATE_WRITE_DATA         5
#define I2C_SLAVE_STATE_READ_DATA          6

extern uint8_t volatile i2c_slave_state;
extern uint8_t volatile i2c_slave_transfer_end_count;



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
extern void i2c_slave_initialize(void);
