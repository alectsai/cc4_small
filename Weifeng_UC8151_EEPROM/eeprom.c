//----------------------------------------------------------------------------
// eeprom.c
//----------------------------------------------------------------------------
#include "all_includes.h"



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
uint32_t eeprom_memory_size;
uint16_t eeprom_page_size;
uint8_t eeprom_power_state;
uint8_t eeprom_power_timeout;
uint8_t eeprom_address_size;



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void eeprom_initialize(void) {
	// two sizes of EEPROM are used.
	//  Most tags use a 256 Kbit part
	//	The 4.2 tag uses a 1Mbit part
	//
	//  The 256 Kbit part used:
	//    CAT25256YI-GT3 from On Semi
	//	  Uses 2 byte addresses
	//    Does NOT have a readable ID
	//
	//  The 512 Kbit part used:
	//    S-25C512AOI-T8T1U4 from SEIKO INSTRUMENTS
	//	  Uses 2 byte addresses
	//    Does NOT have a readable ID
	//
	//  The 1 Mbit part used:
	//    AT25DF011-XMHN-B	from ADESTO
	//	  Uses 3 byte addresses
	//    DOES have a readable ID

	eeprom_address_size = 2;
	eeprom_power(EEPROM_POWER_OFF);
}


//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void eeprom_power(uint8_t state) {

	if (state == EEPROM_POWER_ON) {
		if (eeprom_power_state != EEPROM_POWER_ON) {
			SPI_DIR_MEMORY_CS = 1;
			spi_power(SPI_POWER_ON);

			// This display size uses a GPIO to directly power the EEPROM
			POWER_EEPROM_ENA = 1;
			timer_delay(20);
			SPI_DIR_MEMORY_CS = 0;
			eeprom_power_state = EEPROM_POWER_ON;
		}

		eeprom_power_timeout = EEPROM_POWER_TIMEOUT_COUNT;

	} else {
		__disable_interrupt();

		if (eeprom_power_timeout == 0) {
			SPI_DIR_MEMORY_CS = 1;
			// This display size uses a GPIO to directly power the EEPROM
			POWER_EEPROM_ENA = 0;
			spi_power(SPI_POWER_OFF);
			SPI_DIR_MEMORY_CS = 0;
			eeprom_power_state = EEPROM_POWER_OFF;
		}

		__enable_interrupt();
	}
}



//----------------------------------------------------------------------------
//  If buffer is null, this behave as a clear operation
//----------------------------------------------------------------------------
void eeprom_write(uint32_t address, uint8_t *buffer, uint16_t size) {
	uint16_t i;
	uint32_t mask;
	uint8_t command[4];
	uint8_t timeout;

	eeprom_power(EEPROM_POWER_ON);

	// Determine if data crosses page boundary. If so, break it into two writes
	mask = ~(uint32_t)(eeprom_page_size - 1);
	if ((address & mask) != ((address + size - 1) & mask)) {
		i = eeprom_page_size - (uint16_t)(address & (eeprom_page_size - 1));
		eeprom_write(address,buffer,i);

		if (buffer != NULL) {
			buffer += i;
		}

		eeprom_write(address + i,buffer,size - i);

	} else {
		// Send WREN command
		command[0] = 0x06;
		spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,1,SPI_TRANSFER_NO_CONTINUATION);

		// Write data
		command[0] = 0x02;
		if (eeprom_address_size == 3) {
			command[1] = (address >> 16);
			command[2] = (address >> 8);
			command[3] = address;
			spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,4,SPI_TRANSFER_CONTINUATION);

		} else {
			command[1] = (address >> 8);
			command[2] = address;
			spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,3,SPI_TRANSFER_CONTINUATION);
		}

		spi_transfer(SPI_DEVICE_MEMORY_CS,buffer,NULL,size,SPI_TRANSFER_NO_CONTINUATION);

		// Poll for completion
		timeout = 0;
		while (1) {
			command[0] = 0x05; // RDSR
			command[1] = 0xFF;
			spi_transfer(SPI_DEVICE_MEMORY_CS,command,command,2,SPI_TRANSFER_NO_CONTINUATION);
			if ((command[1] & 0x01) == 0) {
				break;
			}

			++timeout;
			if (timeout > 10) {
				// 10ms maximum wait
				break;
			}

			timer_delay(1);
		}
	}
}



//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
void eeprom_read(uint32_t address, uint8_t *buffer, uint16_t size) {
	uint8_t command[4];

	eeprom_power(EEPROM_POWER_ON);

	command[0] = 0x03;
	if (eeprom_address_size == 3) {
		command[1] = (address >> 16);
		command[2] = (address >> 8);
		command[3] = address;
		spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,4,SPI_TRANSFER_CONTINUATION);

	} else {
		command[1] = (address >> 8);
		command[2] = address;
		spi_transfer(SPI_DEVICE_MEMORY_CS,command,NULL,3,SPI_TRANSFER_CONTINUATION);
	}

	spi_transfer(SPI_DEVICE_MEMORY_CS,NULL,buffer,size,SPI_TRANSFER_NO_CONTINUATION);
}

void eeprom_read_uint32(uint32_t address, uint32_t *value) {
	uint8_t data[4];
	
	eeprom_read(address, data, 4);

	*value = 	((uint32_t)data[0])<<24 |
				((uint32_t)data[1])<<16 |
				((uint32_t)data[2])<<8 |
				((uint32_t)data[3]);
}

void eeprom_write_uint32(uint32_t address, uint32_t value) {
	uint8_t data[4];

	data[0] = value>>24;
	data[1] = value>>16;
	data[2] = value>>8;
	data[3] = value;
	
	eeprom_write(address, data, 4);
}


