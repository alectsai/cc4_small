// uc8151/UC8159 commands
#define UC_CMD_00_PSR			0x00
#define UC_CMD_01_PWR			0x01
#define UC_CMD_02_POF			0x02
#define UC_CMD_03_PFS			0x03
#define UC_CMD_04_PON			0x04
#define UC_CMD_05_PMES			0x05
#define UC_CMD_06_BTST			0x06
#define UC_CMD_07_DSLP			0x07

#define UC_CMD_10_DTM1			0x10
#define UC_CMD_11_DSP			0x11
#define UC_CMD_12_DRF			0x12
#define UC_CMD_13_DTM2			0x13

#define UC_CMD_20_LUTC			0x20
#define UC_CMD_21_LUTWW			0x21
#define UC_CMD_22_LUTBW			0x22
#define UC_CMD_22_LUTR			0x22
#define UC_CMD_23_LUTWB			0x23
#define UC_CMD_23_LUTW			0x23
#define UC_CMD_24_LUTBB			0x24
#define UC_CMD_24_LUTB			0x24

//#define UC_CMD_25_LUT_RED0		0x25 ??
//#define UC_CMD_26_LUT_RED1		0x26 ??
//#define UC_CMD_27_LUT_RED2		0x27 ??
//#define UC_CMD_28_LUT_RED		0x28 ??
//#define UC_CMD_29_LUT_XON		0x29 ??

#define UC_CMD_30_PLL			0x30

#define UC_CMD_40_TSC			0x40
#define UC_CMD_41_TSE			0x41
#define UC_CMD_42_TSW			0x42
#define UC_CMD_43_TSR			0x43

#define UC_CMD_50_CDI			0x50
#define UC_CMD_51_LPD			0x51

#define UC_CMD_60_TCON			0x60
#define UC_CMD_61_TRES			0x61
#define UC_CMD_65_DAM			0x65

#define UC_CMD_70_REV			0x70
#define UC_CMD_71_FLG			0x71

#define UC_CMD_80_AMV			0x80
#define UC_CMD_81_VV			0x81
#define UC_CMD_82_VDCS			0x82

#define UC_CMD_90_PTL			0x90
#define UC_CMD_91_PTIN			0x91
#define UC_CMD_92_PTOUT			0x92

#define UC_CMD_A0_PGM			0xA0
#define UC_CMD_A1_APG			0xA1
#define UC_CMD_A2_ROTP			0xA2

#define UC_CMD_AB_FLASH_WAKE	0xAB

#define UC_CMD_B9_FLASH_SLEEP	0xB9

#define UC_CMD_E0_CCSET			0xE0
#define UC_CMD_E3_PWS			0xE3
#define UC_CMD_E5_FLASH_READ	0xE5

